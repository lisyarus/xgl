#include "common.hpp"

#include <xgl/objects/program.hpp>
#include <xgl/image/white_noise.hpp>
#include <xgl/image/render.hpp>
#include <xgl/resources/shaders/fullscreen.hpp>
#include <xgl/mesh/any.hpp>
#include <xgl/mesh/fullscreen.hpp>

#include <util/clock.hpp>
#include <util/autoname.hpp>

#include <iostream>

struct white_noise_demo
	: demo
{
	util::clock<std::chrono::duration<float>> timer;

	geom::vector_2i window_size;

	std::default_random_engine rng;

	xgl::framebuffer framebuffer;

	xgl::texture_2d texture;
	xgl::texture_2d texture_back;
	xgl::texture_2d texture_vel;

	xgl::program program;
	xgl::program step_program;

	xgl::mesh::any mesh;

	bool paused;
	bool colored;

	white_noise_demo ( );

	void setup_events (xsdl::event_poller_t & poller);

	void step ( );

	void render ( );

	void generate_texture ( );
};

char const fragment_source[] = R"(
#version 330

uniform sampler2D texture;
uniform bool colored;

in vec2 texcoord;

void main ( )
{
	float v = texture2D(texture, texcoord).r;
	if (colored)
		gl_FragColor = vec4(0.5 + 0.5 * cos(v * 2.0 * 3.1415926535), 0.5 + 0.5 * sin(v * 2.0 * 3.1415926535), 0.0, 1.0);
	else
		gl_FragColor = vec4(v, v, v, 1.0);
}
)";

char const step_fragment_source[] = R"(
#version 330

uniform sampler2D texture;
uniform sampler2D texture_vel;
uniform float dt;

in vec2 texcoord;

void main ( )
{
	float v = texture2D(texture, texcoord).r;
	float dv = (texture2D(texture_vel, texcoord).r - 0.5) / 0.5;
	gl_FragColor.r = mod(v + dt * dv, 1.0);
}
)";

white_noise_demo::white_noise_demo ( )
	: rng(seed())
	, program(xgl::program(
			xgl::vertex_shader(xgl::resources::shaders::fullscreen::vertex_glsl),
			xgl::fragment_shader(fragment_source))
		)
	, step_program(xgl::program(
			xgl::vertex_shader(xgl::resources::shaders::fullscreen::vertex_glsl),
			xgl::fragment_shader(step_fragment_source))
		)
	, mesh(xgl::mesh::fullscreen())
	, paused(false)
	, colored(false)
{ }

void white_noise_demo::setup_events (xsdl::event_poller_t & poller)
{
	poller.on_key_down([this](auto, std::int32_t key)
	{
		if (key == SDLK_SPACE)
		{
			paused = !paused;
			timer.restart();
		}

		if (key == SDLK_r)
			generate_texture();

		if (key == SDLK_c)
			colored = !colored;
	});

	poller.on_resize([this](auto, xsdl::window_t::size_t size){
		window_size = {size.width, size.height};
		generate_texture();
	});
}

void white_noise_demo::step ( )
{
	float dt = timer.count();
	timer.restart();

	{
		xgl::image::render_to_texture autoname(framebuffer, texture_back);

		xgl::scope::bind_program autoname(step_program);
		step_program["texture"] = 0;
		step_program["texture_vel"] = 1;
		step_program["dt"] = dt;

		xgl::scope::bind_texture_to_unit autoname(GL_TEXTURE0, texture);
		xgl::scope::bind_texture_to_unit autoname(GL_TEXTURE1, texture_vel);

		mesh.draw();
	}

	std::swap(texture, texture_back);
}

void white_noise_demo::render ( )
{
	if (!paused)
		step();

	xgl::scope::bind_program autoname(program);
	program["texture"] = 0;
	program["colored"] = colored;

	xgl::scope::bind_texture_to_unit autoname(GL_TEXTURE0, texture_back);

	mesh.draw();
}

void white_noise_demo::generate_texture ( )
{
	xgl::image::white_noise(texture, window_size / 16, rng);
	xgl::image::white_noise(texture_vel, window_size / 16, rng);

	texture.set_filters(GL_NEAREST, GL_NEAREST);
	texture_vel.set_filters(GL_NEAREST, GL_NEAREST);

	{
		xgl::scope::bind_texture autoname(texture_back);
		texture_back.load<xgl::pixel_1b>(window_size / 16);
		texture_back.set_filters(GL_NEAREST, GL_NEAREST);
	}

	if (paused)
		step();
}

int main ( )
{
	run_demo<white_noise_demo>();
}
