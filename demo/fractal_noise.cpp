#include "common.hpp"

#include <xgl/objects/program.hpp>
#include <xgl/image/white_noise.hpp>
#include <xgl/image/perlin.hpp>
#include <xgl/image/fractal.hpp>
#include <xgl/image/render.hpp>
#include <xgl/resources/shaders/fullscreen.hpp>
#include <xgl/mesh/any.hpp>
#include <xgl/mesh/fullscreen.hpp>

#include <geom/operations/interval.hpp>

#include <util/clock.hpp>
#include <util/autoname.hpp>

#include <iostream>

struct fractal_noise_demo
	: demo
{
	util::clock<std::chrono::duration<float>> timer;

	geom::vector_2i window_size;

	xgl::image::perlin perlin;
	xgl::image::fractal fractal;

	std::default_random_engine rng;

	xgl::framebuffer framebuffer;

	xgl::texture_2d white_noise;
	xgl::texture_2d white_noise_back;
	xgl::texture_2d white_noise_vel;
	xgl::texture_2d perlin_noise;
	xgl::texture_2d fractal_noise;

	xgl::program program;
	xgl::program step_program;

	xgl::mesh::any mesh;

	bool paused;
	int iterations;
	bool colored;
	bool plasma;

	fractal_noise_demo ( );

	void setup_events (xsdl::event_poller_t & poller);

	void step ( );

	void render ( );

	void generate_texture ( );
};

char const fragment_source[] = R"(
#version 330

uniform sampler2D texture;
uniform bool colored;
uniform float factor;

in vec2 texcoord;

void main ( )
{
	float v = texture2D(texture, texcoord).r * factor;
	if (colored)
		gl_FragColor = vec4(0.5 + 0.5 * cos(v * 2.0 * 3.1415926535), 0.0, 0.5 + 0.5 * sin(v * 2.0 * 3.1415926535), 1.0);
	else
		gl_FragColor = vec4(v, v, v, 1.0);
}
)";

char const step_fragment_source[] = R"(
#version 330

uniform sampler2D texture;
uniform sampler2D texture_vel;
uniform float dt;

in vec2 texcoord;

void main ( )
{
	float v = texture2D(texture, texcoord).r;
	float dv = (texture2D(texture_vel, texcoord).r - 0.5) / 0.5;
	gl_FragColor.r = mod(v + dt * dv, 1.0);
}
)";

fractal_noise_demo::fractal_noise_demo ( )
	: rng(seed())
	, program(xgl::program(
			xgl::vertex_shader(xgl::resources::shaders::fullscreen::vertex_glsl),
			xgl::fragment_shader(fragment_source))
		)
	, step_program(xgl::program(
			xgl::vertex_shader(xgl::resources::shaders::fullscreen::vertex_glsl),
			xgl::fragment_shader(step_fragment_source))
		)
	, mesh(xgl::mesh::fullscreen())
	, paused(false)
	, iterations(3)
	, colored(false)
	, plasma(false)
{ }

void fractal_noise_demo::setup_events (xsdl::event_poller_t & poller)
{
	poller.on_key_down([this](auto, std::int32_t key)
	{
		if (key == SDLK_SPACE)
		{
			paused = !paused;
			timer.restart();
		}

		if (key == SDLK_r)
			generate_texture();

		if (key == SDLK_c)
			colored = !colored;

		if (key == SDLK_p)
			plasma = !plasma;

		if (key == SDLK_UP)
			++iterations;

		if (key == SDLK_DOWN)
			--iterations;

		iterations = geom::clamp(iterations, {1, 10});
	});

	poller.on_resize([this](auto, xsdl::window_t::size_t size){
		window_size = {size.width, size.height};
		generate_texture();
	});
}

void fractal_noise_demo::step ( )
{
	float dt = timer.count();
	timer.restart();

	if (!paused)
	{
		{
			xgl::image::render_to_texture autoname(framebuffer, white_noise_back);

			xgl::scope::bind_program autoname(step_program);
			step_program["texture"] = 0;
			step_program["texture_vel"] = 1;
			step_program["dt"] = dt;

			xgl::scope::bind_texture_to_unit autoname(GL_TEXTURE0, white_noise);
			xgl::scope::bind_texture_to_unit autoname(GL_TEXTURE1, white_noise_vel);

			mesh.draw();
		}

		std::swap(white_noise, white_noise_back);
	}

	perlin(perlin_noise, white_noise, {128, 128});
	perlin_noise.set_filters(GL_NEAREST, GL_NEAREST);

	fractal(fractal_noise, perlin_noise, iterations);
	fractal_noise.set_filters(GL_LINEAR, GL_LINEAR);
}

void fractal_noise_demo::render ( )
{
	step();

	xgl::scope::bind_program autoname(program);
	program["texture"] = 0;
	program["colored"] = colored;
	if (colored && plasma)
		program["factor"] = 8.f;
	else
		program["factor"] = 1.f;

	xgl::scope::bind_texture_to_unit autoname(GL_TEXTURE0, fractal_noise);

	mesh.draw();
}

void fractal_noise_demo::generate_texture ( )
{
	xgl::image::white_noise(white_noise, window_size / 128, rng);
	xgl::image::white_noise(white_noise_vel, window_size / 128, rng);

	white_noise.set_filters(GL_NEAREST, GL_NEAREST);
	white_noise_vel.set_filters(GL_NEAREST, GL_NEAREST);

	{
		xgl::scope::bind_texture autoname(white_noise_back);
		white_noise_back.load<xgl::pixel_1b>(window_size / 128);
		white_noise_back.set_filters(GL_NEAREST, GL_NEAREST);
	}
}

int main ( )
{
	run_demo<fractal_noise_demo>();
}
