#include <xsdl/init.hpp>
#include <xsdl/opengl.hpp>
#include <xsdl/event_poller.hpp>
#include <SDL2/SDL_video.h>

#include <xgl/error.hpp>
#include <xgl/context.hpp>
#include <xgl/state/viewport.hpp>
#include <xgl/objects/buffer.hpp>
#include <xgl/objects/array.hpp>
#include <xgl/objects/program.hpp>

#include <geom/transform/scale.hpp>

#include <util/autoname.hpp>
#include <util/clock.hpp>

#include <random>
#include <chrono>
#include <iostream>

// 2D - marching triangles
int main ( )
{
	xsdl::initializer_t sdl_init;

	xsdl::event_poller_t poller;
	xsdl::gl_window_t window(poller, "Marching triangles via geometry shaders",
		SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE, {600, 600}, {3, 3, false}, 2);

	auto context = xgl::make_global_context(xgl::make_cached_state());
	context->make_current();

	bool quit = false;

	poller.on_quit([&]{ quit = true; });

	poller.on_key_down([&](auto, std::int32_t k)
	{
		if (k == SDLK_ESCAPE)
			quit = true;
	});

	geom::vector_2f const bbox_extent { 0.8f, 0.8f };


	const std::size_t ball_count = 10;

	std::default_random_engine rng (std::chrono::system_clock::now().time_since_epoch().count());

	std::uniform_real_distribution<float> rand_x (-bbox_extent[0], bbox_extent[0]);
	std::uniform_real_distribution<float> rand_y (-bbox_extent[1], bbox_extent[1]);

	std::uniform_real_distribution<float> rand_v (-1.f, 1.f);

	geom::point_2f ball_position[ball_count];
	geom::vector_2f ball_velocity[ball_count];
	float ball_size[ball_count];

	for (std::size_t i = 0; i < ball_count; ++i)
	{
		ball_position[i][0] = rand_x(rng);
		ball_position[i][1] = rand_y(rng);
		ball_velocity[i][0] = rand_v(rng);
		ball_velocity[i][1] = rand_v(rng);
		ball_size[i] = 0.12f;
	}

	int const cell_count = 100;

	xgl::array_buffer quad_buffer;
	{
		std::array<geom::point_2f, 4> quad_vertices
		{{
			{ 0.f, 0.f },
			{ 1.f, 0.f },
			{ 0.f, 1.f },
			{ 1.f, 1.f },
		}};
		quad_buffer.data(quad_vertices);
	}

	xgl::array quad_array;
	{
		xgl::scope::bind_array autoname { quad_array };
		xgl::scope::bind_buffer autoname { quad_buffer };
		quad_array[0].enable().data<geom::point_2f>(0);
	}

	xgl::program main_program { xgl::null };
	{
		const char vs_source[] =
R"(#version 330

uniform vec2 bbox_bl;
uniform vec2 bbox_tr;

uniform int cell_count;

layout (location = 0) in vec2 in_position;

void main ( )
{
	vec2 cell_size = (bbox_tr - bbox_bl) / float(cell_count);

	int cell_x = gl_InstanceID % cell_count;
	int cell_y = gl_InstanceID / cell_count;

	vec2 p = bbox_bl + cell_size * vec2(float(cell_x), float(cell_y)) + in_position * cell_size;
	gl_Position = vec4(p, 0.0, 1.0);
}

)";

		const char gs_source[] =
R"(#version 330

const int ball_count = 10;

layout (triangles) in;
layout (line_strip, max_vertices = 2) out;

uniform mat4 view;
uniform vec2  ball_position[ball_count];
uniform float ball_size[ball_count];

float norm_sqr (vec2 v)
{
	return dot(v, v);
}

float target (vec4 p)
{
	float v = 0.0;
	for (int i = 0; i < ball_count; ++i)
	{
		v += ball_size[i] * ball_size[i] / norm_sqr(p.xy - ball_position[i]);
	}
	return v - 1.0;
}

void main ( )
{
	vec4 p0 = gl_in[0].gl_Position;
	vec4 p1 = gl_in[1].gl_Position;
	vec4 p2 = gl_in[2].gl_Position;

	float v0 = target(p0);
	float v1 = target(p1);
	float v2 = target(p2);

	int mask = 0;
	mask |= (v0 < 0) ? 0 : 1;
	mask |= (v1 < 0) ? 0 : 2;
	mask |= (v2 < 0) ? 0 : 4;

	switch (mask)
	{
	case 0:
	case 7:
		break;
	case 1:
	case 6:
		gl_Position = view * mix(p0, p1, v0 / (v0 - v1));
		EmitVertex();
		gl_Position = view * mix(p0, p2, v0 / (v0 - v2));
		EmitVertex();
		EndPrimitive();
		break;
	case 2:
	case 5:
		gl_Position = view * mix(p1, p0, v1 / (v1 - v0));
		EmitVertex();
		gl_Position = view * mix(p1, p2, v1 / (v1 - v2));
		EmitVertex();
		EndPrimitive();
		break;
	case 3:
	case 4:
		gl_Position = view * mix(p2, p0, v2 / (v2 - v0));
		EmitVertex();
		gl_Position = view * mix(p2, p1, v2 / (v2 - v1));
		EmitVertex();
		EndPrimitive();
		break;
	}
}
)";

		const char fs_source[] =
R"(#version 330

void main ( )
{
	gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
}
)";

		main_program = xgl::program {
			xgl::vertex_shader { vs_source },
			xgl::geometry_shader { gs_source },
			xgl::fragment_shader { fs_source }
		};

		xgl::scope::bind_program autoname { main_program };
		main_program["bbox_bl"] = -bbox_extent;
		main_program["bbox_tr"] =  bbox_extent;
		main_program["cell_count"] = cell_count;
		main_program["view"] = geom::matrix_4x4f::identity();
		glUniform2fv(glGetUniformLocation(main_program.id(), "ball_position"), ball_count, reinterpret_cast<float const *>(ball_position));
		glUniform1fv(glGetUniformLocation(main_program.id(), "ball_size"), ball_count, ball_size);
	}

	poller.on_resize([&](auto, xsdl::window_t::size_t size)
	{
		geom::rectangle_2i viewport {{
			{0, size.width},
			{0, size.height},
		}};
		xgl::viewport(viewport);

		float aspect_ratio = static_cast<float>(size.width) / size.height;

		main_program["view"] = geom::scale<float, 4>({1.f / aspect_ratio, 1.f, 1.f, 1.f}).matrix();
	});

	util::clock<> clock;

	while (!quit)
	{
		poller.poll();

		float const dt = 0.001f;

		for (std::size_t i = 0; i < ball_count; ++i)
		{
			ball_position[i] += ball_velocity[i] * dt;

			if (ball_position[i][0] > bbox_extent[0] - ball_size[i])
			{
				ball_position[i][0] = bbox_extent[0] - ball_size[i];
				ball_velocity[i][0] *= -1.f;
			}

			if (ball_position[i][0] < - bbox_extent[0] + ball_size[i])
			{
				ball_position[i][0] = - bbox_extent[0] + ball_size[i];
				ball_velocity[i][0] *= -1.f;
			}

			if (ball_position[i][1] > bbox_extent[1] - ball_size[i])
			{
				ball_position[i][1] = bbox_extent[1] - ball_size[i];
				ball_velocity[i][1] *= -1.f;
			}

			if (ball_position[i][1] < - bbox_extent[1] + ball_size[i])
			{
				ball_position[i][1] = - bbox_extent[1] + ball_size[i];
				ball_velocity[i][1] *= -1.f;
			}
		}

		glClear(GL_COLOR_BUFFER_BIT);

		{
			xgl::scope::bind_program autoname { main_program };
			glUniform2fv(glGetUniformLocation(main_program.id(), "ball_position"), ball_count, reinterpret_cast<float const *>(ball_position));

			quad_array.draw_instanced(GL_TRIANGLE_STRIP, 4, cell_count * cell_count);
		}

		xgl::throw_error();

		window.swap_buffers();

//		std::cout << "Frame took " << clock.count() << " seconds\n";
		clock.restart();
	}
}
