#include "common.hpp"

#include <geom/operations/vector.hpp>
#include <geom/io/vector.hpp>

#include <util/clock.hpp>

struct colored_screen_demo
	: demo
{
	util::clock<std::chrono::duration<float>> timer;

	float time ( ) const
	{
		return timer.count();
	}

	void render ( );
};

void colored_screen_demo::render ( )
{
	geom::vector_3f a { 1.f, -1.f, 0.f };
	geom::vector_3f b { 0.f, 1.f, -1.f };

	geom::gram_schmidt(a, b);

	geom::vector_3f o { 1.f, 1.f, 1.f };
	o /= 3.f;

	geom::vector_3f c = o + (std::cos(time()) * a + std::sin(time()) * b) / std::sqrt(6.f);

	glClearColor(c[0], c[1], c[2], 1.f);
	glClear(GL_COLOR_BUFFER_BIT);
}

int main ( )
{
	run_demo<colored_screen_demo>();
}
