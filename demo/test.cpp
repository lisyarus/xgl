#include "common.hpp"

#include <util/clock.hpp>

#include <xgl/mesh/mesh.hpp>
#include <xgl/mesh/any.hpp>
#include <xgl/mesh/vertex.hpp>
#include <xgl/shaders/simple.hpp>
#include <xgl/shaders/lit.hpp>
#include <xgl/state/enable.hpp>

#include <geom/transform/rotation.hpp>
#include <geom/transform/scale.hpp>
#include <geom/primitives/rectangle.hpp>
#include <geom/primitives/simplex.hpp>
#include <geom/primitives/sphere.hpp>
#include <geom/primitives/disk.hpp>
#include <geom/primitives/ring.hpp>

struct test_demo
	: demo
{
	util::clock<std::chrono::duration<float>> timer;

	float time ( ) const
	{
		return timer.count();
	}

	test_demo ( );

	void setup_events (xsdl::event_poller_t & poller)
	{
		poller.on_resize([this](xsdl::window_t::id_t, xsdl::window_t::size_t size)
		{
			aspect_ratio = static_cast<float>(size.width) / size.height;
		});
	}

	void render ( );

	float aspect_ratio;

	xgl::mesh::any mesh;
	xgl::shaders::lit_plain shader;
};

test_demo::test_demo ( )
{
//	mesh = xgl::mesh::make_mesh(geom::triangulate<std::uint32_t>(geom::ring<float>(geom::point_2f::zero(), 0.8f, 1.f), 24));
	mesh = xgl::mesh::make_mesh_with_flat_normals(geom::triangulate<std::uint32_t>(geom::sphere<float, 2>(), 64));
}

void test_demo::render ( )
{
	glClearColor(0.f, 0.f, 0.f, 0.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	xgl::scope::enable autoname(GL_DEPTH_TEST);
	xgl::scope::enable autoname(GL_CULL_FACE);

	shader.ambient_color({0.2f, 0.2f, 0.2f, 1.f});
	shader.light_color({1.f, 1.f, 1.f, 1.f});

	float const l = (std::sin(time() * 3.f) * 0.5f + 0.5f) + 1.f / std::sqrt(3.f);
	shader.light_position({l, l, -l, 1.f});

	shader.color({1.f, 1.f, 0.f, 1.f});
	shader.modelview(geom::matrix_4x4f::identity()
		* geom::plane_rotation<float, 4>(0, 2, time() * 0.5f).matrix()
		* geom::scale<float, 4>({1.f, 1.f, -1.f, 1.f}).matrix()
	);
	shader.projection(geom::matrix_4x4f::identity()
		* geom::scale<float, 4>({1.f / aspect_ratio, 1.f, 1.f, 1.f}).matrix()
	);


	xgl::scope::bind_program autoname(shader.program());
	mesh.draw();
}

int main ( )
{
	run_demo<test_demo>();
}
