#include <xsdl/init.hpp>
#include <xsdl/opengl.hpp>
#include <xsdl/event_poller.hpp>
#include <SDL2/SDL_video.h>

#include <xgl/error.hpp>
#include <xgl/context.hpp>
#include <xgl/state/viewport.hpp>
#include <xgl/objects/buffer.hpp>
#include <xgl/objects/array.hpp>
#include <xgl/objects/program.hpp>
#include <xgl/state/enable.hpp>

#include <geom/transform/rotation.hpp>
#include <geom/transform/translation.hpp>
#include <geom/transform/perspective.hpp>
#include <geom/math.hpp>

#include <util/autoname.hpp>
#include <util/clock.hpp>

#include <random>
#include <chrono>
#include <iostream>

struct camera
{
	float a, b;
	float d;

	camera ( )
		: a { 0.f }
		, b { 0.f }
		, d { 3.f }
	{ }

	geom::matrix_4x4f matrix ( ) const
	{
		return geom::matrix_4x4f::identity()
			* geom::translation<float, 3>({0.f, 0.f, -d}).homogeneous_matrix()
			* geom::homogeneous(geom::plane_rotation<float, 3>(1, 2, b).matrix())
			* geom::homogeneous(geom::plane_rotation<float, 3>(2, 0, a).matrix())
			;
	}

	geom::vector_3f direction ( ) const
	{
		return {
			- std::cos(b) * std::sin(a),
			std::sin(b),
			std::cos(b) * std::cos(a),
		};
	}

	geom::point_3f position ( ) const
	{
		return geom::point_3f::zero() - direction() * d;
	}
};


void setup_controls (camera & cam, xsdl::event_poller_t & event_poller)
{
	event_poller.on_mouse_move([&cam, &event_poller](xsdl::window_t::id_t, geom::point_2i, geom::vector_2i delta){
		if (event_poller.middle_button_down())
		{
			cam.a += delta[0] * 0.01f;
			cam.b += delta[1] * 0.01f;
		}
	});

	event_poller.on_wheel([&cam](xsdl::window_t::id_t, double delta){
		cam.d *= std::pow(0.8f, delta);
	});
}

// 3D - marching tetrahedra
int main ( )
{
	xsdl::initializer_t sdl_init;

	xsdl::event_poller_t poller;
	xsdl::gl_window_t window(poller, "Marching tetrahedra via geometry shaders",
		SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE, {600, 600}, {3, 3, false});

	window.vsync(false);

	auto context = xgl::make_global_context(xgl::make_cached_state());
	context->make_current();

	bool quit = false;
	bool paused = false;

	geom::matrix_4x4f projection = geom::matrix_4x4f::identity();

	geom::vector_3f const collide_extent { 0.8f, 0.8f, 0.8f };

	constexpr std::size_t ball_count = 12;

	std::default_random_engine rng;

	auto rand_pos = [&rng]
	{
		return geom::point_3f {
			std::uniform_real_distribution<float>(-0.5f, 0.5f)(rng),
			std::uniform_real_distribution<float>(-0.5f, 0.5f)(rng),
			std::uniform_real_distribution<float>(-0.5f, 0.5f)(rng),
		};
	};

	auto rand_vel = [&rng]
	{
		return 3.f * geom::vector_3f {
			std::uniform_real_distribution<float>(-0.5f, 0.5f)(rng),
			std::uniform_real_distribution<float>(-0.5f, 0.5f)(rng),
			std::uniform_real_distribution<float>(-0.5f, 0.5f)(rng),
//			std::uniform_real_distribution<float>(-1.f, 1.f)(rng),
//			std::uniform_real_distribution<float>(-1.f, 1.f)(rng),
//			std::uniform_real_distribution<float>(-1.f, 1.f)(rng),
		};
	};

	auto rand_size = [&rng]
	{
		return std::uniform_real_distribution<float>(0.2f, 0.3f)(rng);
	};

	geom::vector_3f colors[6] =
	{
		{ 1.f, 0.f, 0.f },
		{ 0.f, 1.f, 0.f },
		{ 0.f, 0.f, 1.f },
		{ 0.f, 1.f, 1.f },
		{ 1.f, 0.f, 1.f },
		{ 1.f, 1.f, 0.f },
	};

	geom::point_3f ball_position[ball_count];
	geom::vector_3f ball_velocity[ball_count];
	geom::vector_3f ball_color[ball_count];
	float ball_size[ball_count];

	for (std::size_t i = 0; i < ball_count; ++i)
	{
		ball_position[i] = rand_pos();
		ball_velocity[i] = rand_vel();
		ball_color[i] = colors[i % 6];
		ball_size[i] = 0.2f;
	}

	camera cam;
	setup_controls(cam, poller);

	poller.on_quit([&]{ quit = true; });

	poller.on_key_down([&](auto, std::int32_t k)
	{
		if (k == SDLK_ESCAPE)
			quit = true;

		if (k == SDLK_SPACE)
			paused = !paused;
	});

	poller.on_resize([&](auto, xsdl::window_t::size_t size)
	{
		geom::rectangle_2i viewport {{
			{0, size.width},
			{0, size.height},
		}};
		xgl::viewport(viewport);

		float aspect_ratio = static_cast<float>(size.width) / size.height;

		projection = geom::perspective<float, 3>(geom::pi / 3.f, aspect_ratio, 0.01f, 100.f).homogeneous_matrix();
	});

	xgl::array_buffer cell_buffer;
	{
		geom::point_3f cell_vertices[]
		{
			{ 0.f, 0.f, 0.f },
			{ 1.f, 0.f, 0.f },
			{ 1.f, 1.f, 0.f },
			{ 1.f, 1.f, 1.f },

			{ 0.f, 0.f, 0.f },
			{ 0.f, 1.f, 0.f },
			{ 1.f, 1.f, 0.f },
			{ 1.f, 1.f, 1.f },

			{ 0.f, 0.f, 0.f },
			{ 0.f, 1.f, 0.f },
			{ 0.f, 1.f, 1.f },
			{ 1.f, 1.f, 1.f },

			{ 0.f, 0.f, 0.f },
			{ 0.f, 0.f, 1.f },
			{ 0.f, 1.f, 1.f },
			{ 1.f, 1.f, 1.f },

			{ 0.f, 0.f, 0.f },
			{ 0.f, 0.f, 1.f },
			{ 1.f, 0.f, 1.f },
			{ 1.f, 1.f, 1.f },

			{ 0.f, 0.f, 0.f },
			{ 1.f, 0.f, 0.f },
			{ 1.f, 0.f, 1.f },
			{ 1.f, 1.f, 1.f },

		};
		cell_buffer.data(cell_vertices);
	}

	xgl::array cell_array;
	{
		xgl::scope::bind_array autoname { cell_array };
		xgl::scope::bind_buffer autoname { cell_buffer };
		cell_array[0].enable().data<geom::point_3f>(0);
	}

	xgl::program main_program { xgl::null };
	{
		const char vs_source[] =
R"(#version 330

uniform vec3 bbox0;
uniform vec3 bbox1;
uniform int cell_count;

layout (location = 0) in vec3 in_position;

void main ( )
{
	int id = gl_InstanceID;
	int cell_x = id % cell_count;
	id = id / cell_count;
	int cell_y = id % cell_count;
	id = id / cell_count;
	int cell_z = id;

	vec3 p = (in_position + vec3(ivec3(cell_x, cell_y, cell_z))) * (bbox1 - bbox0) / cell_count + bbox0;

	gl_Position = vec4(p, 1.0);
}
)";

		const char gs_source[] =
R"(#version 330

uniform mat4 transform;

const int ball_count = 12;

uniform vec3 ball_position[ball_count];
uniform vec3 ball_color[ball_count];
uniform float ball_size[ball_count];

layout (lines_adjacency) in;
layout (triangle_strip, max_vertices = 6) out;

out vec3 position;
out vec3 normal;
out vec3 color;

const vec3 b0 = vec3(0.5, 0.0, 0.0);
const vec3 b1 = vec3(-0.5, 0.0, 0.0);

const vec3 c0 = vec3(1.0, 0.0, 0.0);
const vec3 c1 = vec3(0.0, 0.0, 1.0);

float norm_sqr (vec3 v)
{
	return dot(v, v);
}

vec4 target (vec4 p)
{
	float v = 0.0;
	vec3 c = vec3(0.0, 0.0, 0.0);

	for (int i = 0; i < ball_count; ++i)
	{
		float w = ball_size[i] * ball_size[i] / norm_sqr(p.xyz - ball_position[i]);
		v += w;
		c += ball_color[i] * w;
	}

	return vec4(c / v, v - 1.0);
}

vec3 tgrad (vec4 p)
{
	vec3 g = vec3(0.0, 0.0, 0.0);

	for (int i = 0; i < ball_count; ++i)
	{
		vec3 d = p.xyz - ball_position[i];
		float r2 = dot(d, d);

		g += - ball_size[i] * ball_size[i] * 2.0 * d / r2 / r2;
	}

	return g;
}

vec4 mmix (vec4 p0, float v0, vec4 p1, float v1)
{
	return mix(p0, p1, v0 / (v0 - v1));
}

vec3 cmix (vec3 c0, float v0, vec3 c1, float v1)
{
	return mix(c0, c1, v0 / (v0 - v1));
}

void emit (vec4 p, vec3 c)
{
	vec3 g = tgrad(p);
	vec4 f = target(p);
	for (int i = 0; i < 1; ++i)
	{
		p.xyz -= g * f.w / dot(g, g);
		g = tgrad(p);
		f = target(p);
	}
	gl_Position = transform * p;
	position = p.xyz;
	color = f.xyz;
	normal = normalize(g);
	EmitVertex();
}

void main ( )
{
	vec4 p0 = gl_in[0].gl_Position;
	vec4 p1 = gl_in[1].gl_Position;
	vec4 p2 = gl_in[2].gl_Position;
	vec4 p3 = gl_in[3].gl_Position;

	color = vec3(1.0, 1.0, 1.0);

	vec4 f0 = target(p0);
	vec4 f1 = target(p1);
	vec4 f2 = target(p2);
	vec4 f3 = target(p3);

	float v0 = f0.w;
	float v1 = f1.w;
	float v2 = f2.w;
	float v3 = f3.w;

	vec3 c0 = f0.xyz;
	vec3 c1 = f1.xyz;
	vec3 c2 = f2.xyz;
	vec3 c3 = f3.xyz;

	int mask = 0;
	mask |= (v0 < 0) ? 0 : 1;
	mask |= (v1 < 0) ? 0 : 2;
	mask |= (v2 < 0) ? 0 : 4;
	mask |= (v3 < 0) ? 0 : 8;

	switch (mask)
	{
		case 0:
		case 15:
			break;
		case 1:
		case 14:
		{
			vec4 t0 = mmix(p0, v0, p1, v1);
			vec4 t1 = mmix(p0, v0, p2, v2);
			vec4 t2 = mmix(p0, v0, p3, v3);

			vec3 q0 = cmix(c0, v0, c1, v1);
			vec3 q1 = cmix(c0, v0, c2, v2);
			vec3 q2 = cmix(c0, v0, c3, v3);

			emit(t0, q0);
			emit(t1, q1);
			emit(t2, q2);
			EndPrimitive();
			break;
		}
		case 2:
		case 13:
		{
			vec4 t0 = mmix(p1, v1, p0, v0);
			vec4 t1 = mmix(p1, v1, p2, v2);
			vec4 t2 = mmix(p1, v1, p3, v3);

			vec3 q0 = cmix(c1, v1, c0, v0);
			vec3 q1 = cmix(c1, v1, c2, v2);
			vec3 q2 = cmix(c1, v1, c3, v3);

			emit(t0, q0);
			emit(t1, q1);
			emit(t2, q2);
			EndPrimitive();
			break;
		}
		case 3:
		case 12:
		{
			vec4 t0 = mmix(p0, v0, p2, v2);
			vec4 t1 = mmix(p0, v0, p3, v3);
			vec4 t2 = mmix(p1, v1, p2, v2);
			vec4 t3 = mmix(p1, v1, p3, v3);

			vec3 q0 = cmix(c0, v0, c2, v2);
			vec3 q1 = cmix(c0, v0, c3, v3);
			vec3 q2 = cmix(c1, v1, c2, v2);
			vec3 q3 = cmix(c1, v1, c3, v3);

			emit(t0, q0);
			emit(t1, q1);
			emit(t2, q2);
			EndPrimitive();
			emit(t1, q1);
			emit(t2, q2);
			emit(t3, q3);
			EndPrimitive();
			break;
		}
		case 4:
		case 11:
		{
			vec4 t0 = mmix(p2, v2, p0, v0);
			vec4 t1 = mmix(p2, v2, p1, v1);
			vec4 t2 = mmix(p2, v2, p3, v3);

			vec3 q0 = cmix(c2, v2, c0, v0);
			vec3 q1 = cmix(c2, v2, c1, v1);
			vec3 q2 = cmix(c2, v2, c3, v3);

			emit(t0, q0);
			emit(t1, q1);
			emit(t2, q2);
			EndPrimitive();
			break;
		}
		case 5:
		case 10:
		{
			vec4 t0 = mmix(p0, v0, p1, v1);
			vec4 t1 = mmix(p0, v0, p3, v3);
			vec4 t2 = mmix(p2, v2, p1, v1);
			vec4 t3 = mmix(p2, v2, p3, v3);

			vec3 q0 = cmix(c0, v0, c1, v1);
			vec3 q1 = cmix(c0, v0, c3, v3);
			vec3 q2 = cmix(c2, v2, c1, v1);
			vec3 q3 = cmix(c2, v2, c3, v3);

			emit(t0, q0);
			emit(t1, q1);
			emit(t2, q2);
			EndPrimitive();
			emit(t1, q1);
			emit(t2, q2);
			emit(t3, q3);
			EndPrimitive();
			break;
		}
		case 6:
		case 9:
		{
			vec4 t0 = mmix(p1, v1, p0, v0);
			vec4 t1 = mmix(p1, v1, p3, v3);
			vec4 t2 = mmix(p2, v2, p0, v0);
			vec4 t3 = mmix(p2, v2, p3, v3);

			vec3 q0 = cmix(c1, v1, c0, v0);
			vec3 q1 = cmix(c1, v1, c3, v3);
			vec3 q2 = cmix(c2, v2, c0, v0);
			vec3 q3 = cmix(c2, v2, c3, v3);

			emit(t0, q0);
			emit(t1, q1);
			emit(t2, q2);
			EndPrimitive();
			emit(t1, q1);
			emit(t2, q2);
			emit(t3, q3);
			EndPrimitive();
			break;
		}
		case 7:
		case 8:
		{
			vec4 t0 = mmix(p3, v3, p0, v0);
			vec4 t1 = mmix(p3, v3, p1, v1);
			vec4 t2 = mmix(p3, v3, p2, v2);

			vec3 q0 = cmix(c3, v3, c0, v0);
			vec3 q1 = cmix(c3, v3, c1, v1);
			vec3 q2 = cmix(c3, v3, c2, v2);

			emit(t0, q0);
			emit(t1, q1);
			emit(t2, q2);
			EndPrimitive();
			break;
		}
	}
}

)";

		const char fs_source[] =
R"(#version 330

uniform vec3 camera;

in vec3 position;
in vec3 normal;
in vec3 color;

void main ( )
{
	vec4 light = vec4(2.0, 3.0, 4.0, 1.0);

	float ambient = 0.3;

	float illumination = max(0.0, dot(normalize(normal), normalize(position * light.w - light.xyz)));

	vec3 camera_ray = normalize(camera - position);

	float specular = max(0.0, pow(dot(camera_ray, normal), 80.0)) * illumination;

	//float specular = exp(-(1.0-illumination)*(1.0-illumination)*10000.0);

	gl_FragColor = vec4(color, 1.0) * (ambient + illumination * (1 - ambient)) + vec4(1.0, 1.0, 1.0, 1.0) * specular;
}
)";

		main_program = xgl::program {
			xgl::vertex_shader { vs_source },
			xgl::geometry_shader { gs_source },
			xgl::fragment_shader { fs_source }
		};

		xgl::scope::bind_program autoname { main_program };
		glUniform3fv(main_program.uniform_location("ball_color"), ball_count, reinterpret_cast<float const *>(ball_color));
		glUniform1fv(main_program.uniform_location("ball_size"), ball_count, reinterpret_cast<float const *>(ball_size));
	}

	util::clock<> clock;

	while (!quit)
	{
		poller.poll();

		float const dt = 0.01f;

		if (!paused) for (std::size_t i = 0; i < ball_count; ++i)
		{
			auto r = ball_position[i] - geom::point_3f::zero();

			ball_velocity[i] -= dt * r * std::exp(std::pow(geom::norm(r), 2.f));
			ball_position[i] += ball_velocity[i] * dt;

			continue;

			if (ball_position[i][0] > collide_extent[0] - ball_size[i])
			{
				ball_position[i][0] = collide_extent[0] - ball_size[i];
				ball_velocity[i][0] *= -1.f;
			}

			if (ball_position[i][0] < - collide_extent[0] + ball_size[i])
			{
				ball_position[i][0] = - collide_extent[0] + ball_size[i];
				ball_velocity[i][0] *= -1.f;
			}

			if (ball_position[i][1] > collide_extent[1] - ball_size[i])
			{
				ball_position[i][1] = collide_extent[1] - ball_size[i];
				ball_velocity[i][1] *= -1.f;
			}

			if (ball_position[i][1] < - collide_extent[1] + ball_size[i])
			{
				ball_position[i][1] = - collide_extent[1] + ball_size[i];
				ball_velocity[i][1] *= -1.f;
			}

			if (ball_position[i][2] > collide_extent[2] - ball_size[i])
			{
				ball_position[i][2] = collide_extent[2] - ball_size[i];
				ball_velocity[i][2] *= -1.f;
			}

			if (ball_position[i][2] < - collide_extent[2] + ball_size[i])
			{
				ball_position[i][2] = - collide_extent[2] + ball_size[i];
				ball_velocity[i][2] *= -1.f;
			}
		}

		geom::vector_3f bbox0 { 1.f, 1.f, 1.f };
		geom::vector_3f bbox1 { -1.f, -1.f, -1.f };

		float sum_squared_sizes = 0.f;

		for (std::size_t i = 0; i < ball_count; ++i)
		{
			sum_squared_sizes += ball_size[i] * ball_size[i];

			for (std::size_t k : { 0, 1, 2 })
			{
				if (ball_position[i][k] < bbox0[k])
					bbox0[k] = ball_position[i][k];
				if (ball_position[i][k] > bbox1[k])
					bbox1[k] = ball_position[i][k];
			}
		}

		float const bbox_extra = std::sqrt(sum_squared_sizes);

		float max_extent = 0.f;

		for (std::size_t k : { 0, 1, 2 })
		{
			bbox0[k] -= bbox_extra;
			bbox1[k] += bbox_extra;

			if (bbox1[k] - bbox0[k] > max_extent)
				max_extent = bbox1[k] - bbox0[k];
		}

		int const cell_count = static_cast<int>(max_extent * 20.f);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		{
			xgl::scope::enable autoname { GL_DEPTH_TEST };

			xgl::scope::bind_program autoname { main_program };

			main_program["transform"] = projection * cam.matrix();
			main_program["bbox0"] = bbox0;
			main_program["bbox1"] = bbox1;
			main_program["cell_count"] = cell_count;
			main_program["camera"] = cam.position();
			glUniform3fv(main_program.uniform_location("ball_position"), ball_count, reinterpret_cast<float const *>(ball_position));

			cell_array.draw_instanced(GL_LINES_ADJACENCY, 24, cell_count * cell_count * cell_count);
		}

		xgl::throw_error();

		window.swap_buffers();

		std::cout << "Frame took " << clock.count() << " seconds\n";
		clock.restart();
	}
}
