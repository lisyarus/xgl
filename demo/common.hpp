#pragma once

#include <xsdl/init.hpp>
#include <xsdl/opengl.hpp>
#include <xsdl/event_poller.hpp>
#include <SDL2/SDL_video.h>

#include <xgl/error.hpp>
#include <xgl/context.hpp>
#include <xgl/state/viewport.hpp>

#include <util/autoname.hpp>

#include <random>
#include <chrono>

static auto seed ( )
{
	return std::chrono::system_clock::now().time_since_epoch().count();
}

struct demo
{
	void setup_events (xsdl::event_poller_t & poller) { }

	void render ( ) { }
};

template <typename Demo, typename ... Args>
void run_demo (Args && ... args)
{
	xsdl::initializer_t init;

	xsdl::event_poller_t poller;
	xsdl::gl_window_t window(poller, "xgl test", SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN);

	auto context = xgl::make_global_context(xgl::make_cached_state());
	context->make_current();

	Demo d(std::forward<Args>(args)...);
	d.setup_events(poller);

	bool quit = false;

	poller.on_quit([&]{ quit = true; });

	poller.on_key_down([&](auto, std::int32_t k)
	{
		if (k == SDLK_ESCAPE)
			quit = true;
	});

	poller.on_resize([&](auto, xsdl::window_t::size_t size)
	{
		geom::rectangle_2i viewport {{
			{0, size.width},
			{0, size.height},
		}};
		xgl::viewport(viewport);
	});

	while (!quit)
	{
		poller.poll();

		d.render();

		xgl::throw_error();

		window.swap_buffers();
	}
}
