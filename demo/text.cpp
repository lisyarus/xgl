#include "common.hpp"

#include <xgl/objects/program.hpp>
#include <xgl/text/freetype.hpp>
#include <xgl/text/mesh.hpp>
#include <xgl/state/enable.hpp>

#include <util/autoname.hpp>
#include <util/unicode.hpp>

struct text_demo
	: demo
{
	xgl::text::glyph_atlas atlas;
	xgl::text::mesh mesh;

	xgl::program program;

	text_demo ( );

	void setup_events (xsdl::event_poller_t & poller);

	void render ( );
};

char const vertex_source[] = R"(
#version 330

layout (location = 0) in vec2 position_in;
layout (location = 1) in vec2 texcoord_in;

out vec2 texcoord;

void main ( )
{
	gl_Position = vec4(position_in, 0.0, 1.0);
	texcoord = texcoord_in;
}
)";

char const fragment_source[] = R"(
#version 330

uniform sampler2D texture;
uniform vec2 texsize;

in vec2 texcoord;

void main ( )
{
	float v = texture2D(texture, texcoord).r;
	gl_FragColor = vec4(1.0, 1.0, 1.0, v);
}
)";

static const int font_size = 16;

text_demo::text_demo ( )
	: program(xgl::program(
			xgl::vertex_shader(vertex_source),
			xgl::fragment_shader(fragment_source))
		)
{
	{
		xgl::text::freetype::library ft_library;
		atlas = ft_library.load(XGL_DEMO_FONT, 0, font_size, 0, false);
	}
}

void text_demo::setup_events (xsdl::event_poller_t & poller)
{
	poller.on_resize([this](auto, xsdl::window_t::size_t size){
		xgl::text::options opts;
		opts.start = {-0.5f, 0.5f};
		opts.size =
		{
			static_cast<float>(2 * font_size) / size.width,
			static_cast<float>(2 * font_size) / size.height
		};
		opts.line_spacing = 1.25f;
		opts.up = false;
		opts.max_row_width = 1.f;

		std::string text =
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit,"
			"sed do eiusmod tempor incididunt ut labore et dolore magna"
			"aliqua. Ut enim ad minim veniam, quis nostrud exercitation"
			"ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis"
			"aute irure dolor in reprehenderit in voluptate velit esse cillum"
			"dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat"
			"non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
		;

		mesh.update(atlas, util::from_utf8(text), opts);
	});
}

void text_demo::render ( )
{
	glClearColor(0.f, 0.f, 0.75f, 0.f);
	glClear(GL_COLOR_BUFFER_BIT);

	xgl::scope::enable autoname(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	xgl::scope::bind_program autoname(program);
	program["texture"] = 0;

	xgl::scope::bind_texture_to_unit autoname(GL_TEXTURE0, atlas.texture);

	mesh.render();
}

int main ( )
{
	run_demo<text_demo>();
}
