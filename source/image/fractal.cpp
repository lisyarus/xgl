#include <xgl/image/fractal.hpp>
#include <xgl/image/render.hpp>
#include <xgl/objects/program.hpp>
#include <xgl/state/enable.hpp>
#include <xgl/pixel.hpp>
#include <xgl/error.hpp>
#include <xgl/mesh/fullscreen.hpp>

#include <xgl/resources/shaders/image/fractal.hpp>
#include <xgl/resources/shaders/fullscreen.hpp>

#include <util/autoname.hpp>

#include <iostream>

namespace xgl::image
{

	static program create_fractal_program ( )
	{
		return program(
				vertex_shader(resources::shaders::fullscreen::vertex_glsl),
				fragment_shader(resources::shaders::image::fractal::fragment_glsl)
			);
	}

	fractal::fractal ( )
		: program_(create_fractal_program())
		, mesh_(mesh::fullscreen())
	{ }

	void fractal::operator() (xgl::texture_2d & output, xgl::texture_2d const & noise, size_t iterations)
	{
		output.load<pixel_1b>(noise.size());

		{
			image::render_to_texture autoname(framebuffer_, output);

			xgl::scope::bind_program autoname(program_);

			program_["noise"] = 0;
			program_["iterations"] = iterations;

			scope::bind_texture_to_unit autoname(GL_TEXTURE0, noise);

			mesh_.draw();
		}
	}

}
