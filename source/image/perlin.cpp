#include <xgl/image/perlin.hpp>
#include <xgl/image/render.hpp>
#include <xgl/objects/program.hpp>
#include <xgl/state/enable.hpp>
#include <xgl/pixel.hpp>
#include <xgl/error.hpp>
#include <xgl/mesh/fullscreen.hpp>

#include <xgl/resources/shaders/image/perlin.hpp>
#include <xgl/resources/shaders/fullscreen.hpp>

#include <util/autoname.hpp>

#include <iostream>

namespace xgl::image
{

	static program create_perlin_program ( )
	{
		return program(
			vertex_shader(resources::shaders::fullscreen::vertex_glsl),
			fragment_shader(resources::shaders::image::perlin::fragment_glsl)
			);
	}

	perlin::perlin ( )
		: program_(create_perlin_program())
		, mesh_(mesh::fullscreen())
	{ }

	void perlin::operator() (xgl::texture_2d & output, xgl::texture_2d const & noise, geom::vector_2i size)
	{
		output.load<pixel_4b>({size[0] * noise.size()[0], size[1] * noise.size()[1]});

		{
			image::render_to_texture autoname(framebuffer_, output);

			xgl::scope::bind_program autoname(program_);

			program_["noise_size"] = noise.size();
			program_["noise_delta"] = geom::vector_2f { 1.0f / noise.width(), 1.0f / noise.height() };
			program_["grad"] = 0;

			scope::bind_texture_to_unit autoname(GL_TEXTURE0, noise);

			mesh_.draw();
		}
	}

}
