#include <xgl/mesh/ball_stick.hpp>
#include <xgl/mesh/vertex.hpp>

#include <geom/math.hpp>
#include <util/functional.hpp>
#include <geom/operations/vector.hpp>

namespace xgl::mesh
{

	namespace
	{

		XGL_DECLARE_VERTEX(vertex,
			((position, geom::point_3f, 0))
			((normal, geom::vector_3f, 1))
			((color, geom::vector_4f, 2))
		);

	}

	indexed_mesh ball_stick (
		geom::triangulation<geom::point_3f, 1> const & graph,
		float ball_radius,
		float stick_ragius,
		std::size_t quality,
		std::function<geom::vector_4f(std::size_t)> const & vertex_color,
		std::function<geom::vector_4f(std::size_t, float)> const & edge_color)
	{
		indexed_mesh m;

		std::vector<vertex> vertices;
		std::vector<std::uint32_t> indices;

		for (std::size_t vertex_index = 0; vertex_index < graph.vertices.size(); ++vertex_index)
		{
			auto color = vertex_color(vertex_index);
			auto position = graph.vertices[vertex_index];

			auto base_index = vertices.size();

			auto push_vertex = [&](geom::vector_3f n){
				vertices.push_back({ position + n * ball_radius, n, color });
			};

			auto push_triangle = [&](std::size_t i1, std::size_t i2, std::size_t i3)
			{
				indices.push_back(static_cast<std::uint32_t>(base_index + i1));
				indices.push_back(static_cast<std::uint32_t>(base_index + i2));
				indices.push_back(static_cast<std::uint32_t>(base_index + i3));
			};

			auto index_at = [quality](std::size_t i, std::size_t j)
			{
				return 2 + (j - 1) * (4 * quality) + (i % (4 * quality));
			};

			push_vertex({0.f, 0.f, -1.f});
			push_vertex({0.f, 0.f,  1.f});

			for (std::size_t j = 1; j < 2 * quality; ++j)
			{
				float const aj = (geom::pi_f * j * 0.5f) / quality;

				for (std::size_t i = 0; i < 4 * quality; ++i)
				{
					float const ai = (geom::pi_f * i * 0.5f) / quality;

					geom::vector_3f n
					{
						std::cos(ai) * std::sin(aj),
						std::sin(ai) * std::sin(aj),
						-std::cos(aj)
					};

					push_vertex(n);
				}
			}

			// bottom cap
			for (std::size_t i = 0; i < 4 * quality; ++i)
			{
				push_triangle(0, index_at(i + 1, 1), index_at(i, 1));
			}

			// top cap
			for (std::size_t i = 0; i < 4 * quality; ++i)
			{
				push_triangle(1, index_at(i, 2 * quality - 1), index_at(i + 1, 2 * quality - 1));
			}

			// middle strip
			for (std::size_t j = 1; j < 2 * quality - 1; ++j)
			{
				for (std::size_t i = 0; i < 4 * quality; ++i)
				{
					push_triangle(index_at(i, j), index_at(i + 1, j), index_at(i + 1, j + 1));
					push_triangle(index_at(i, j), index_at(i + 1, j + 1), index_at(i, j + 1));
				}
			}
		}

		for (std::size_t edge_index = 0; edge_index < graph.simplices.size(); ++edge_index)
		{
			auto const & edge = graph.simplices[edge_index];

			auto base_index = vertices.size();

			auto push_triangle = [&](std::size_t i1, std::size_t i2, std::size_t i3)
			{
				indices.push_back(static_cast<std::uint32_t>(base_index + i1));
				indices.push_back(static_cast<std::uint32_t>(base_index + i2));
				indices.push_back(static_cast<std::uint32_t>(base_index + i3));
			};

			auto index_at = [quality](std::size_t i, std::size_t j)
			{
				return 2 * (i % (4 * quality)) + j;
			};

			geom::point_3f position0 = graph.vertices[edge[0]];
			geom::point_3f position1 = graph.vertices[edge[1]];

			geom::vector_3f delta = position1 - position0;

			geom::vector_3f n1 = geom::normalized(geom::ort(delta));
			geom::vector_3f n2 = geom::normalized(delta ^ n1);

			for (std::size_t i = 0; i < 4 * quality; ++i)
			{
				float const ai = (geom::pi_f * i * 0.5f) / quality;

				geom::vector_3f n = std::cos(ai) * n1 + std::sin(ai) * n2;

				vertices.push_back({ position0 + n * stick_ragius, n, edge_color(edge_index, 0.f) });
				vertices.push_back({ position1 + n * stick_ragius, n, edge_color(edge_index, 1.f) });
			}

			for (std::size_t i = 0; i < 4 * quality; ++i)
			{
				push_triangle(index_at(i, 0), index_at(i + 1, 0), index_at(i + 1, 1));
				push_triangle(index_at(i, 0), index_at(i + 1, 1), index_at(i, 1));
			}
		}

		m.mode(GL_TRIANGLES);
		m.load(vertices);
		m.load_indices(indices);
		return m;
	}

	indexed_mesh ball_stick (
		geom::triangulation<geom::point_3f, 1> const & graph,
		float ball_radius,
		float stick_ragius,
		std::size_t quality,
		std::function<geom::vector_4f(std::size_t)> const & vertex_color)
	{
		auto interpolated_color = [&](std::size_t edge_index, float t){
			auto edge = graph.simplices[edge_index];
			return geom::lerp(vertex_color(edge[0]), vertex_color(edge[1]), t);
		};

		return ball_stick(graph, ball_radius, stick_ragius, quality, vertex_color, interpolated_color);
	}

	indexed_mesh ball_stick (
		geom::triangulation<geom::point_3f, 1> const & graph,
		float ball_radius,
		float stick_ragius,
		std::size_t quality,
		geom::vector_4f const & color)
	{
		auto constant_color = util::constant(color);

		return ball_stick(graph, ball_radius, stick_ragius, quality, constant_color, constant_color);
	}

}
