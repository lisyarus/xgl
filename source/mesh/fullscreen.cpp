#include <xgl/mesh/fullscreen.hpp>
#include <xgl/mesh/vertex.hpp>

#include <geom/alias/point.hpp>

namespace xgl::mesh
{

	namespace
	{

		XGL_DECLARE_VERTEX(vertex,
			((position, geom::point_2i, 0))
		);

	}

	mesh fullscreen ( )
	{
		mesh m;

		vertex vertices[]
		{
			{{ -1, -1 }},
			{{  1, -1 }},
			{{ -1,  1 }},
			{{  1,  1 }}
		};

		m.load(vertices);
		m.mode(GL_TRIANGLE_STRIP);
		return m;
	}

}
