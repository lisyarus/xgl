#include <xgl/mesh/axes.hpp>
#include <xgl/mesh/vertex.hpp>

#include <geom/alias/point.hpp>
#include <geom/alias/vector.hpp>

namespace xgl::mesh
{

	namespace
	{

		XGL_DECLARE_VERTEX(vertex,
			((position, geom::point_3i, 0))
			((color, geom::vector_4i, 1))
		);

	}

	mesh axes ( )
	{
		mesh m;

		vertex vertices[]
		{
			{{ 0, 0, 0 }, { 1, 0, 0, 1 }},
			{{ 1, 0, 0 }, { 1, 0, 0, 1 }},
			{{ 0, 0, 0 }, { 0, 1, 0, 1 }},
			{{ 0, 1, 0 }, { 0, 1, 0, 1 }},
			{{ 0, 0, 0 }, { 0, 0, 1, 1 }},
			{{ 0, 0, 1 }, { 0, 0, 1, 1 }},
		};

		m.load(vertices);
		m.mode(GL_LINES);
		return m;
	}

}
