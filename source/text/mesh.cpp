#include <xgl/text/mesh.hpp>
#include <xgl/mesh/vertex.hpp>

#include <util/autoname.hpp>

namespace xgl::text
{

	namespace
	{

		XGL_DECLARE_VERTEX(vertex,
			((position, geom::point_2f, 0))
			((texcoord, geom::point_2f, 1))
		);

	}

	void mesh::update (xgl::text::glyph_atlas const & atlas, std::u32string const & str, options const & opts)
	{
		auto sized = [&opts](geom::vector_2f v) -> geom::vector_2f
		{
			return { v[0] * opts.size[0], v[1] * opts.size[1] };
		};

		std::vector<vertex> vertices;
		std::vector<std::uint32_t> indices;

		geom::point_2f pos = opts.start;
		if (!opts.up)
			pos[1] -= opts.line_spacing;

		auto get_vertex = [&](xgl::text::glyph_data const & data, geom::vector_2f const & v)
		{
			geom::vector_2f off = geom::vector_2f{ data.offset[0], opts.up ? 1.f - data.size[1] - data.offset[1] : data.offset[1] }
				+ geom::vector_2f{ data.size[0] * v[0], data.size[1] * (opts.up ? v[1] : 1.f - v[1]) };
			return vertex{
				pos + sized(off),
				data.texcoords(v)
			};
		};

		auto newline = [&]
		{
			if (opts.up)
				pos = { opts.start[0], pos[1] + opts.size[1] * opts.line_spacing };
			else
				pos = { opts.start[0], pos[1] - opts.size[1] * opts.line_spacing };
		};

		for (char32_t c : str)
		{
			if (c == U'\n')
			{
				newline();
			}
			else if (auto data = atlas.at(c))
			{
				if (opts.max_row_width && (pos[0] - opts.start[0] + data->size[0] * opts.size[0] > *opts.max_row_width))
					newline();

				std::uint32_t index = vertices.size();

				vertices.push_back(get_vertex(*data, {0.f, 0.f}));
				vertices.push_back(get_vertex(*data, {1.f, 0.f}));
				vertices.push_back(get_vertex(*data, {0.f, 1.f}));
				vertices.push_back(get_vertex(*data, {1.f, 1.f}));

				indices.push_back(index + 0);
				indices.push_back(index + 1);
				indices.push_back(index + 3);

				indices.push_back(index + 0);
				indices.push_back(index + 3);
				indices.push_back(index + 2);

				pos += sized(data->advance * opts.spacing);
			}
		}
/*
		vertices.clear();
		indices.clear();

		vertices.push_back({ {0.f, 0.f}, {0.f, 0.f} });
		vertices.push_back({ {600.f, 0.f}, {1.f, 0.f} });
		vertices.push_back({ {0.f, 600.f}, {0.f, 1.f} });
		vertices.push_back({ {600.f, 600.f}, {1.f, 1.f} });

		indices.push_back(0);
		indices.push_back(1);
		indices.push_back(2);

		indices.push_back(2);
		indices.push_back(1);
		indices.push_back(3);
*/
		mesh_.load(vertices);
		mesh_.load_indices(indices);
		mesh_.mode(GL_TRIANGLES);
	}

	void mesh::render ( )
	{
		mesh_.draw();
	}

}
