#include <xgl/context.hpp>
#include <xgl/get.hpp>

#include <unordered_map>
#include <vector>

namespace xgl
{

	thread_local context * context::current = nullptr;

	void context::make_current ( )
	{
		current = this;
	}

	namespace
	{

		struct cached_state
			: state
		{
			enum_t cull_face_;
			std::unordered_map<enum_t, int> pixel_storei_;
			geom::interval_d depth_range_;
			enum_t depth_func_;
			geom::vector_4f blend_color_;
			enum_t blend_func_src_rgb_;
			enum_t blend_func_src_alpha_;
			enum_t blend_func_dst_rgb_;
			enum_t blend_func_dst_alpha_;
			enum_t blend_equation_;
			std::unordered_map<enum_t, bool> enable_;
			std::unordered_map<enum_t, bool> enable_client_state_;
			geom::rectangle_2i viewport_;
			std::size_t active_texture_;
			std::vector<std::unordered_map<enum_t, id_t>> bind_texture_;
			std::unordered_map<enum_t, id_t> bind_renderbuffer_;
			std::unordered_map<enum_t, id_t> bind_framebuffer_;
			std::unordered_map<enum_t, id_t> bind_buffer_;
			id_t bind_array_;
			id_t use_program_;

			cached_state ( )
			{
				init_misc();
				init_enable();
				init_enable_client_state();
				init_viewport();
				init_bind_texture();
				init_bind_renderbuffer();
				init_bind_framebuffer();
				init_bind_buffer();
				init_bind_array();
				init_use_program();
			}

			void init_misc ( )
			{
				cull_face_ = GL_BACK;
				pixel_storei_[GL_PACK_ALIGNMENT] = 4;
				pixel_storei_[GL_UNPACK_ALIGNMENT] = 4;
				depth_range_ = { 0.0, 1.0 };
				depth_func_ = GL_LESS;
				blend_color_ = { 0.f, 0.f, 0.f, 0.f };
				blend_func_src_rgb_ = GL_ONE;
				blend_func_src_alpha_ = GL_ONE;
				blend_func_dst_rgb_ = GL_ZERO;
				blend_func_dst_alpha_ = GL_ZERO;
				blend_equation_ = GL_FUNC_ADD;
			}

			void init_enable ( )
			{
				enum_t enabled[] = {
					GL_DITHER,
					GL_MULTISAMPLE,
				};

				enum_t disabled[] = {
					GL_BLEND,
					GL_CLIP_DISTANCE0,
					GL_CLIP_DISTANCE1,
					GL_CLIP_DISTANCE2,
					GL_CLIP_DISTANCE3,
					GL_CLIP_DISTANCE4,
					GL_CLIP_DISTANCE5,
					GL_CLIP_DISTANCE6,
					GL_CLIP_DISTANCE7,
					GL_COLOR_LOGIC_OP,
					GL_CULL_FACE,
					GL_DEBUG_OUTPUT,
					GL_DEBUG_OUTPUT_SYNCHRONOUS,
					GL_DEPTH_CLAMP,
					GL_DEPTH_TEST,
					GL_FRAMEBUFFER_SRGB,
					GL_LINE_SMOOTH,
					GL_POLYGON_OFFSET_FILL,
					GL_POLYGON_OFFSET_LINE,
					GL_POLYGON_OFFSET_POINT,
					GL_POLYGON_SMOOTH,
					GL_PRIMITIVE_RESTART,
					GL_PRIMITIVE_RESTART_FIXED_INDEX,
					GL_RASTERIZER_DISCARD,
					GL_SAMPLE_ALPHA_TO_COVERAGE,
					GL_SAMPLE_ALPHA_TO_ONE,
					GL_SAMPLE_COVERAGE,
					GL_SAMPLE_SHADING,
					GL_SAMPLE_MASK,
					GL_SCISSOR_TEST,
					GL_STENCIL_TEST,
					GL_TEXTURE_CUBE_MAP_SEAMLESS,
					GL_PROGRAM_POINT_SIZE,

					GL_TEXTURE_1D,
					GL_TEXTURE_2D,
					GL_TEXTURE_3D,
					GL_TEXTURE_1D_ARRAY,
					GL_TEXTURE_2D_ARRAY,
					GL_TEXTURE_RECTANGLE,
					GL_TEXTURE_CUBE_MAP,
					GL_TEXTURE_CUBE_MAP_ARRAY,
					GL_TEXTURE_BUFFER,
					GL_TEXTURE_2D_MULTISAMPLE,
					GL_TEXTURE_2D_MULTISAMPLE_ARRAY,
				};

				for (auto c : enabled)
					enable_[c] = true;

				for (auto c : disabled)
					enable_[c] = false;
			}

			void init_enable_client_state ( )
			{
				enum_t disabled[] = {
					GL_COLOR_ARRAY,
					GL_EDGE_FLAG_ARRAY,
					GL_FOG_COORD_ARRAY,
					GL_INDEX_ARRAY,
					GL_NORMAL_ARRAY,
					GL_SECONDARY_COLOR_ARRAY,
					GL_TEXTURE_COORD_ARRAY,
					GL_VERTEX_ARRAY,
				};

				for (auto c : disabled)
					enable_client_state_[c] = false;
			}

			void init_viewport ( )
			{
				GLint v[4];
				glGetIntegerv(GL_VIEWPORT, v);
				viewport_[0].inf = v[0];
				viewport_[1].inf = v[1];
				viewport_[0].sup = v[0] + v[2];
				viewport_[1].sup = v[1] + v[3];
			}

			void init_bind_texture ( )
			{
				enum_t types[] = {
					GL_TEXTURE_1D,
					GL_TEXTURE_2D,
					GL_TEXTURE_3D,
					GL_TEXTURE_1D_ARRAY,
					GL_TEXTURE_2D_ARRAY,
					GL_TEXTURE_RECTANGLE,
					GL_TEXTURE_CUBE_MAP,
					GL_TEXTURE_CUBE_MAP_ARRAY,
					GL_TEXTURE_BUFFER,
					GL_TEXTURE_2D_MULTISAMPLE,
					GL_TEXTURE_2D_MULTISAMPLE_ARRAY,
				};

				active_texture_ = 0;
				bind_texture_.resize(get<GLint>(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS));

				for (auto & b : bind_texture_)
					for (auto t : types)
						b[t] = 0;
			}

			void init_bind_renderbuffer ( )
			{
				enum_t types[] = {
					GL_RENDERBUFFER,
				};

				for (auto t : types)
					bind_renderbuffer_[t] = 0;
			}

			void init_bind_framebuffer ( )
			{
				enum_t types[] = {
					GL_DRAW_FRAMEBUFFER,
					GL_READ_FRAMEBUFFER,
					GL_FRAMEBUFFER,
				};

				for (auto t : types)
					bind_framebuffer_[t] = 0;
			}

			void init_bind_buffer ( )
			{
				// NB: GL_ELEMENT_ARRAY_BUFFER binding is NOT cached,
				// since it is part of VAO state

				enum_t types[] = {
					GL_ARRAY_BUFFER,
					GL_ATOMIC_COUNTER_BUFFER,
					GL_COPY_READ_BUFFER,
					GL_COPY_WRITE_BUFFER,
					GL_DISPATCH_INDIRECT_BUFFER,
					GL_DRAW_INDIRECT_BUFFER,
					GL_PIXEL_PACK_BUFFER,
					GL_PIXEL_UNPACK_BUFFER,
					GL_QUERY_BUFFER,
					GL_SHADER_STORAGE_BUFFER,
					GL_TEXTURE_BUFFER,
					GL_TRANSFORM_FEEDBACK_BUFFER,
					GL_UNIFORM_BUFFER,
				};

				for (auto t : types)
					bind_buffer_[t] = 0;
			}

			void init_bind_array ( )
			{
				bind_array_ = 0;
			}

			void init_use_program ( )
			{
				use_program_ = 0;
			}

			void cull_face (enum_t face) override
			{
				if (cull_face_ != face)
				{
					cull_face_ = face;
					glCullFace(face);
				}
			}

			enum_t cull_face ( ) const override
			{
				return cull_face_;
			}

			void pixel_store (enum_t cap, int value) override
			{
				if (pixel_storei_[cap] != value)
				{
					pixel_storei_[cap] = value;
					glPixelStorei(cap, value);
				}
			}

			int pixel_store (enum_t cap) const
			{
				return pixel_storei_.at(cap);
			}

			void depth_range (geom::interval_d const & range) override
			{
				depth_range_ = range;
				glDepthRange(range.inf, range.sup);
			}

			geom::interval_d depth_range ( ) const override
			{
				return depth_range_;
			}

			void depth_func (enum_t func) override
			{
				if (depth_func_ != func)
				{
					depth_func_ = func;
					glDepthFunc(func);
				}
			}

			enum_t depth_func ( ) const override
			{
				return depth_func_;
			}

			void blend_color (geom::vector_4f const & color) override
			{
				blend_color_ = color;
				glBlendColor(color[0], color[1], color[2], color[3]);
			}

			geom::vector_4f blend_color ( ) const override
			{
				return blend_color_;
			}

			void blend_func (enum_t src_func, enum_t dst_func) override
			{
				blend_func(src_func, dst_func, src_func, dst_func);
			}

			void blend_func (enum_t src_rgb_func, enum_t dst_rgb_func, enum_t src_alpha_func, enum_t dst_alpha_func) override
			{
				if (false
					|| blend_func_src_rgb_ != src_rgb_func
					|| blend_func_dst_rgb_ != dst_rgb_func
					|| blend_func_src_alpha_ != src_alpha_func
					|| blend_func_dst_alpha_ != dst_alpha_func
					)
				{
					blend_func_src_rgb_ = src_rgb_func;
					blend_func_dst_rgb_ = dst_rgb_func;
					blend_func_src_alpha_ = src_alpha_func;
					blend_func_dst_alpha_ = dst_alpha_func;
					glBlendFuncSeparate(src_rgb_func, dst_rgb_func, src_alpha_func, dst_alpha_func);
				}
			}

			void blend_func (blend_func_data const & data) override
			{
				blend_func(data.src.rgb, data.dst.rgb, data.src.alpha, data.dst.alpha);
			}

			blend_func_data blend_func( ) const override
			{
				return { { blend_func_src_rgb_, blend_func_src_alpha_ }, { blend_func_dst_rgb_, blend_func_dst_alpha_ } };
			}

			void blend_equation (enum_t equation) override
			{
				if (blend_equation_ != equation)
				{
					blend_equation_ = equation;
					glBlendEquation(equation);
				}
			}

			enum_t blend_equation ( ) const override
			{
				return blend_equation_;
			}

			void enable (enum_t cap, bool value) override
			{
				if (enable_[cap] != value)
				{
					enable_[cap] = value;
					if (value)
						glEnable(cap);
					else
						glDisable(cap);
				}
			}

			bool enable (enum_t cap) const override
			{
				return enable_.at(cap);
			}

			void enable_client_state (enum_t cap, bool value) override
			{
				if (enable_client_state_[cap] != value)
				{
					enable_client_state_[cap] = value;
					if (value)
						glEnableClientState(cap);
					else
						glDisableClientState(cap);
				}
			}

			bool enable_client_state (enum_t cap) const override
			{
				return enable_client_state_.at(cap);
			}

			geom::rectangle_2i viewport ( ) const override
			{
				return viewport_;
			}

			void viewport (geom::rectangle_2i rect) override
			{
				// no sense tracking vieport change here
				// if the user changes the viewport, say, every frame - not our deal

				viewport_ = rect;
				glViewport(rect[0].inf, rect[1].inf, rect[0].length(), rect[1].length());
			}

			void bind_texture (enum_t type, id_t id) override
			{
				if (bind_texture_[active_texture_][type] != id)
				{
					bind_texture_[active_texture_][type] = id;
					glBindTexture(type, id);
				}
			}

			id_t bind_texture (enum_t type) const override
			{
				return bind_texture_[active_texture_].at(type);
			}

			void active_texture (enum_t unit) override
			{
				active_texture_ = unit - GL_TEXTURE0;
				glActiveTexture(unit);
			}

			enum_t active_texture ( ) const override
			{
				return active_texture_ + GL_TEXTURE0;
			}

			void bind_renderbuffer (enum_t type, id_t id) override
			{
				if (bind_renderbuffer_[type] != id)
				{
					bind_renderbuffer_[type] = id;
					glBindRenderbuffer(type, id);
				}
			}

			id_t bind_renderbuffer (enum_t type) const override
			{
				return bind_renderbuffer_.at(type);
			}

			void bind_framebuffer (enum_t type, id_t id) override
			{
				if (bind_framebuffer_[type] != id)
				{
					bind_framebuffer_[type] = id;
					glBindFramebuffer(type, id);
				}
			}

			id_t bind_framebuffer (enum_t type) const override
			{
				return bind_framebuffer_.at(type);
			}

			void bind_buffer (enum_t type, id_t id) override
			{
				if (bind_buffer_[type] != id)
				{
					bind_buffer_[type] = id;
					glBindBuffer(type, id);
				}
			}

			id_t bind_buffer (enum_t type) const override
			{
				return bind_buffer_.at(type);
			}

			void bind_array (id_t id) override
			{
				if (bind_array_ != id)
				{
					bind_array_ = id;
					glBindVertexArray(id);
				}
			}

			id_t bind_array ( ) const override
			{
				return bind_array_;
			}

			void use_program (id_t id) override
			{
				if (use_program_ != id)
				{
					use_program_ = id;
					glUseProgram(id);
				}
			}

			id_t use_program ( ) const override
			{
				return use_program_;
			}
		};

		struct global_context_t
			: context
		{
			global_context_t (std::unique_ptr<xgl::state> state)
				: state_(std::move(state))
			{ }

			std::unique_ptr<xgl::state> state_;

			void make_current ( ) override
			{
				context::make_current();
			}

			xgl::state & state ( )
			{
				return *state_;
			}

			xgl::state const & state ( ) const
			{
				return *state_;
			}
		};

	}

	std::unique_ptr<state> make_cached_state ( )
	{
		return std::make_unique<cached_state>();
	}

	std::unique_ptr<context> make_global_context (std::unique_ptr<state> state)
	{
		return std::make_unique<global_context_t>(std::move(state));
	}

}
