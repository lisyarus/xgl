#include <xgl/shaders/simple.hpp>

#include <xgl/resources/shaders/simple_plain.hpp>
#include <xgl/resources/shaders/simple_colored.hpp>
#include <xgl/resources/shaders/simple_plain_transformed.hpp>
#include <xgl/resources/shaders/simple_colored_transformed.hpp>

namespace xgl::shaders
{

	program simple_plain::make_program ( )
	{
		return {
				vertex_shader(resources::shaders::simple_plain::vertex_glsl),
				fragment_shader(resources::shaders::simple_plain::fragment_glsl)
			};
	}

	void simple_plain::color (geom::vector_4f const & color)
	{
		program_["color"] = color;
	}

	program simple_colored::make_program ( )
	{
		return {
				vertex_shader(resources::shaders::simple_colored::vertex_glsl),
				fragment_shader(resources::shaders::simple_colored::fragment_glsl)
			};
	}

	program simple_plain_transformed::make_program ( )
	{
		return {
				vertex_shader(resources::shaders::simple_plain_transformed::vertex_glsl),
				fragment_shader(resources::shaders::simple_plain_transformed::fragment_glsl)
			};
	}

	void simple_plain_transformed::color (geom::vector_4f const & color)
	{
		program_["color"] = color;
	}

	void simple_plain_transformed::transform (geom::matrix_4x4f const & transform)
	{
		program_["transform"] = transform;
	}

	program simple_colored_transformed::make_program ( )
	{
		return {
				vertex_shader(resources::shaders::simple_colored_transformed::vertex_glsl),
				fragment_shader(resources::shaders::simple_colored_transformed::fragment_glsl)
			};
	}

	void simple_colored_transformed::transform (geom::matrix_4x4f const & transform)
	{
		program_["transform"] = transform;
	}

}
