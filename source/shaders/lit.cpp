#include <xgl/shaders/lit.hpp>
#include <xgl/resources/shaders/lit_plain.hpp>
#include <xgl/resources/shaders/lit_colored.hpp>

namespace xgl::shaders
{

	xgl::program lit_plain::make_program ( )
	{
		return {
			vertex_shader(resources::shaders::lit_plain::vertex_glsl),
			fragment_shader(resources::shaders::lit_plain::fragment_glsl),
		};
	}

	void lit_plain::color (geom::vector_4f const & color)
	{
		program_["color"] = color;
	}

	void lit_plain::modelview (geom::matrix_4x4f const & transform)
	{
		program_["modelview"] = transform;
	}

	void lit_plain::projection (geom::matrix_4x4f const & transform)
	{
		program_["projection"] = transform;
	}

	void lit_plain::ambient_color (geom::vector_4f const & color)
	{
		program_["ambient_color"] = color;
	}

	void lit_plain::light_color (geom::vector_4f const & color)
	{
		program_["light_color"] = color;
	}

	void lit_plain::light_position (geom::vector_4f const & position)
	{
		program_["light_position"] = position;
	}

	xgl::program lit_colored::make_program ( )
	{
		return {
			vertex_shader(resources::shaders::lit_colored::vertex_glsl),
			fragment_shader(resources::shaders::lit_colored::fragment_glsl),
		};
	}

	void lit_colored::modelview (geom::matrix_4x4f const & transform)
	{
		program_["modelview"] = transform;
	}

	void lit_colored::projection (geom::matrix_4x4f const & transform)
	{
		program_["projection"] = transform;
	}

	void lit_colored::ambient_color (geom::vector_4f const & color)
	{
		program_["ambient_color"] = color;
	}

	void lit_colored::light_color (geom::vector_4f const & color)
	{
		program_["light_color"] = color;
	}

	void lit_colored::light_position (geom::vector_4f const & position)
	{
		program_["light_position"] = position;
	}

}
