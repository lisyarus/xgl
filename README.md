A small OpenGL-wrapper library for C++

* RAII for OpenGL objects (textures, buffers, shaders, framebuffers, ...)
* RAII for scoped OpenGL state changes
* Shaders utilities
* Texture generation
* Freetype integration
