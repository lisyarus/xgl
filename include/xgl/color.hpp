#pragma once

#include <geom/alias/vector.hpp>

namespace xgl::color
{

	constexpr geom::vector_4f black { 0.f, 0.f, 0.f, 1.f };
	constexpr geom::vector_4f white { 1.f, 1.f, 1.f, 1.f };

	constexpr geom::vector_4f red     { 1.f, 0.f, 0.f, 1.f };
	constexpr geom::vector_4f green   { 0.f, 1.f, 0.f, 1.f };
	constexpr geom::vector_4f blue    { 0.f, 0.f, 1.f, 1.f };

	constexpr geom::vector_4f cyan    { 0.f, 1.f, 1.f, 1.f };
	constexpr geom::vector_4f magenta { 1.f, 0.f, 1.f, 1.f };
	constexpr geom::vector_4f yellow  { 1.f, 1.f, 0.f, 1.f };

}
