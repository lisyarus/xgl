#pragma once

#include <xgl/objects/texture.hpp>

#include <unordered_map>
#include <optional>

namespace xgl
{

	template <typename Texture, typename Key, typename Value>
	struct basic_atlas
	{
		using texture_type = Texture;
		using key_type = Key;
		using value_type = Value;

		texture_type texture;
		std::unordered_map<key_type, value_type> map;

		std::optional<Value> at (Key const & key) const
		{
			auto it = map.find(key);
			if (it == map.end())
				return std::nullopt;
			else
				return it->second;
		}
	};

	template <typename Key>
	using atlas_1d = basic_atlas<texture_1d, Key, geom::rectangle_1f>;

	template <typename Key>
	using atlas_2d = basic_atlas<texture_2d, Key, geom::rectangle_2f>;

	template <typename Key>
	using atlas_3d = basic_atlas<texture_3d, Key, geom::rectangle_3f>;

}
