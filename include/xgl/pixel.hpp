#pragma once

#include <xgl/gl.hpp>

#include <cstdint>

namespace xgl
{

	template <typename Pixel>
	struct pixel_traits;

	struct pixel_1b
	{
		unsigned char r;
	};

	struct pixel_2b
	{
		unsigned char r, g;
	};

	struct pixel_3b
	{
		unsigned char r, g, b;
	};

	struct pixel_4b
	{
		unsigned char r, g, b, a;
	};

	struct pixel_1f
	{
		float r;

		pixel_1f & operator = (float r)
		{
			this->r = r;
			return *this;
		}

		operator float ( ) const
		{
			return r;
		}
	};

	struct pixel_2f
	{
		float r, g;
	};

	struct pixel_depth_4b
	{
		std::uint32_t d;
	};

	struct pixel_depth_1f
	{
		float d;
	};

	struct pixel_depth_3b_stencil_1b
	{
		std::uint32_t v;
	};

	template < >
	struct pixel_traits<pixel_1b>
	{
		static const enum_t gl_internal_format = GL_RED;
		static const enum_t gl_format = GL_RED;
		static const enum_t gl_type = GL_UNSIGNED_BYTE;
	};

	template < >
	struct pixel_traits<pixel_2b>
	{
		static const enum_t gl_internal_format = GL_RG;
		static const enum_t gl_format = GL_RG;
		static const enum_t gl_type = GL_UNSIGNED_BYTE;
	};

	template < >
	struct pixel_traits<pixel_3b>
	{
		static const enum_t gl_internal_format = GL_RGB;
		static const enum_t gl_format = GL_RGB;
		static const enum_t gl_type = GL_UNSIGNED_BYTE;
	};

	template < >
	struct pixel_traits<pixel_4b>
	{
		static const enum_t gl_internal_format = GL_RGBA;
		static const enum_t gl_format = GL_RGBA;
		static const enum_t gl_type = GL_UNSIGNED_BYTE;
	};

	template < >
	struct pixel_traits<pixel_1f>
	{
		static const enum_t gl_internal_format = GL_R32F;
		static const enum_t gl_format = GL_RED;
		static const enum_t gl_type = GL_FLOAT;
	};

	template < >
	struct pixel_traits<pixel_2f>
	{
		static const enum_t gl_internal_format = GL_RG32F;
		static const enum_t gl_format = GL_RG;
		static const enum_t gl_type = GL_FLOAT;
	};

	template < >
	struct pixel_traits<pixel_depth_4b>
	{
		static const enum_t gl_internal_format = GL_DEPTH_COMPONENT32;
		static const enum_t gl_format = GL_DEPTH_COMPONENT;
		static const enum_t gl_type = GL_UNSIGNED_INT;
	};

	template < >
	struct pixel_traits<pixel_depth_1f>
	{
		static const enum_t gl_internal_format = GL_DEPTH_COMPONENT32F;
		static const enum_t gl_format = GL_DEPTH_COMPONENT;
		static const enum_t gl_type = GL_FLOAT;
	};

	template < >
	struct pixel_traits<pixel_depth_3b_stencil_1b>
	{
		static const enum_t gl_internal_format = GL_DEPTH24_STENCIL8;
		static const enum_t gl_format = GL_DEPTH_STENCIL;
		static const enum_t gl_type = GL_UNSIGNED_INT;
	};

}
