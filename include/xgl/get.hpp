#pragma once

#include <xgl/gl.hpp>

namespace xgl
{

	template <typename T>
	T get (enum_t parameter);

	template < >
	inline GLboolean get<GLboolean> (enum_t parameter)
	{
		GLboolean value;
		glGetBooleanv(parameter, &value);
		return value;
	}

	template < >
	inline GLint get<GLint> (enum_t parameter)
	{
		GLint value;
		glGetIntegerv(parameter, &value);
		return value;
	}

	template < >
	inline GLfloat get<GLfloat> (enum_t parameter)
	{
		GLfloat value;
		glGetFloatv(parameter, &value);
		return value;
	}

}
