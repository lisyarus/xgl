#pragma once

#include <cstddef>

namespace xgl::mesh
{

	template <std::size_t Index, typename Type, std::size_t Offset>
	struct attribute
	{
		static constexpr std::size_t index = Index;
		using type = Type;
		static constexpr std::size_t offset = Offset;
	};

}
