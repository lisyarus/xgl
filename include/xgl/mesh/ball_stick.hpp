#pragma once

#include <xgl/mesh/mesh.hpp>

#include <geom/primitives/triangulation.hpp>

#include <functional>

namespace xgl::mesh
{

	indexed_mesh ball_stick (
		geom::triangulation<geom::point_3f, 1> const & graph,
		float ball_radius,
		float stick_ragius,
		std::size_t quality,
		std::function<geom::vector_4f(std::size_t)> const & vertex_color,
		std::function<geom::vector_4f(std::size_t, float)> const & edge_color);

	indexed_mesh ball_stick (
		geom::triangulation<geom::point_3f, 1> const & graph,
		float ball_radius,
		float stick_ragius,
		std::size_t quality,
		std::function<geom::vector_4f(std::size_t)> const & vertex_color);

	indexed_mesh ball_stick (
		geom::triangulation<geom::point_3f, 1> const & graph,
		float ball_radius,
		float stick_ragius,
		std::size_t quality,
		geom::vector_4f const & color);

}
