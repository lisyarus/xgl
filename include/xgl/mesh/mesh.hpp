#pragma once

#include <xgl/objects/array.hpp>
#include <xgl/objects/buffer.hpp>
#include <xgl/mesh/attribute.hpp>

#include <geom/primitives/triangulation.hpp>
#include <geom/alias/point.hpp>
#include <geom/operations/vector.hpp>
#include <geom/operations/simplex.hpp>

#include <typeinfo>

namespace xgl::mesh
{

	struct mesh
	{
		mesh ( )
			: count_(0)
			, draw_mode_(0)
			, vertex_type_(nullptr)
		{ }

		mesh (null_tag tag)
			: array_(tag)
			, buffer_(tag)
			, count_(0)
			, draw_mode_(0)
			, vertex_type_(nullptr)
		{ }

		enum_t mode (enum_t m)
		{
			std::swap(draw_mode_, m);
			return m;
		}

		enum_t mode ( ) const
		{
			return draw_mode_;
		}

		template <typename Vertex>
		void load (Vertex const * data, std::size_t size)
		{
			buffer_.data(data, size);
			count_ = static_cast<size_t>(size);

			std::type_info const * new_vertex_type = &typeid(Vertex);
			if (!vertex_type_ || *vertex_type_ != *new_vertex_type)
			{
				vertex_type_ = new_vertex_type;
				array_.attributes<Vertex>(buffer_);
			}
		}

		template <typename Array>
		void load (Array const & array)
		{
			load(xgl::detail::array_data(array), xgl::detail::array_size(array));
		}

		void draw ( )
		{
			if (count_ == 0) return;
			if (draw_mode_ == 0) return;

			array_.draw(draw_mode_, count_);
		}

	private:
		array array_;
		array_buffer buffer_;
		size_t count_;
		enum_t draw_mode_;

		std::type_info const * vertex_type_;
	};

	struct indexed_mesh
	{
		indexed_mesh ( )
			: count_(0)
			, draw_mode_(0)
			, vertex_type_(nullptr)
			, index_type_(0)
		{
			array_.indices(index_buffer_);
		}

		indexed_mesh (null_tag tag)
			: array_(tag)
			, buffer_(tag)
			, index_buffer_(tag)
			, count_(0)
			, draw_mode_(0)
			, vertex_type_(nullptr)
			, index_type_(0)
		{ }

		enum_t mode (enum_t m)
		{
			std::swap(draw_mode_, m);
			return m;
		}

		enum_t mode ( ) const
		{
			return draw_mode_;
		}

		template <typename Vertex>
		void load (Vertex const * data, std::size_t size)
		{
			buffer_.data(data, size);

			std::type_info const * new_vertex_type = &typeid(Vertex);
			if (!vertex_type_ || *vertex_type_ != *new_vertex_type)
			{
				vertex_type_ = new_vertex_type;
				array_.attributes<Vertex>(buffer_);
			}
		}

		template <typename Array>
		void load (Array const & array)
		{
			load(xgl::detail::array_data(array), xgl::detail::array_size(array));
		}

		template <typename Index>
		void load_indices (Index const * data, std::size_t size)
		{
			index_buffer_.data(data, size);
			count_ = static_cast<size_t>(size);
			index_type_ = to_gl_type<Index>::value;
		}

		template <typename Index, std::size_t N>
		void load_indices (std::array<Index, N> const * data, std::size_t size)
		{
			index_buffer_.data(data, size);
			count_ = static_cast<size_t>(size) * N;
			index_type_ = to_gl_type<Index>::value;
		}

		template <typename Array>
		void load_indices (Array const & array)
		{
			load_indices(xgl::detail::array_data(array), xgl::detail::array_size(array));
		}

		void draw ( )
		{
			if (count_ == 0) return;
			if (draw_mode_ == 0) return;

			array_.draw_indexed(draw_mode_, count_, index_type_);
		}

	private:
		array array_;
		array_buffer buffer_;
		array_buffer index_buffer_;
		size_t count_;
		enum_t draw_mode_;

		std::type_info const * vertex_type_;
		enum_t index_type_;
	};

	namespace detail
	{

		template <std::size_t N>
		constexpr enum_t simplex_dimension_to_draw_mode ( );

		template < >
		constexpr enum_t simplex_dimension_to_draw_mode<0> ( )
		{
			return GL_POINTS;
		}

		template < >
		constexpr enum_t simplex_dimension_to_draw_mode<1> ( )
		{
			return GL_LINES;
		}

		template < >
		constexpr enum_t simplex_dimension_to_draw_mode<2> ( )
		{
			return GL_TRIANGLES;
		}

	}

	template <typename Point, std::size_t N, typename Index>
	indexed_mesh make_mesh (geom::triangulation<Point, N, Index> const & triangulation)
	{
		struct vertex
		{
			using attributes = std::tuple<attribute<0, Point, 0>>;

			Point position;
		};

		indexed_mesh m;
		m.load(reinterpret_cast<vertex const *>(triangulation.vertices.data()), triangulation.vertices.size());
		m.load_indices(triangulation.simplices);
		m.mode(detail::simplex_dimension_to_draw_mode<N>());
		return m;
	}

	template <std::size_t NormalAttributeIndex = 1, typename T, typename Index>
	mesh make_mesh_with_flat_normals (geom::triangulation<geom::point<T, 3>, 2, Index> const & triangulation)
	{
		struct vertex
		{
			using attributes = std::tuple<
				attribute<0, geom::point<T, 3>, 0>,
				attribute<NormalAttributeIndex, geom::vector<T, 3>, sizeof(geom::point<T, 3>)>
			>;

			geom::point<T, 3> position;
			geom::vector<T, 3> normal;
		};

		std::vector<vertex> vertices;

		for (auto const & s : triangulation.simplices)
		{
			auto v0 = triangulation.vertices[s[0]];
			auto v1 = triangulation.vertices[s[1]];
			auto v2 = triangulation.vertices[s[2]];

			auto n = geom::normalized((v1 - v0) ^ (v2 - v0));

			vertices.push_back({v0, n});
			vertices.push_back({v1, n});
			vertices.push_back({v2, n});
		}

		mesh m;
		m.load(vertices);
		m.mode(GL_TRIANGLES);
		return m;
	}

	namespace detail
	{

		template <typename T>
		struct default_triangle_weight
		{
			T operator() (geom::point<T, 3> const & p0, geom::point<T, 3> const & p1, geom::point<T, 3> const & p2)
			{
				return geom::measure(geom::simplex<geom::point<T, 3>, 2>{ p0, p1, p2 });
			}
		};

	}

	template <std::size_t NormalAttributeIndex = 1, typename T, typename Index, typename Weight = detail::default_triangle_weight<T>>
	indexed_mesh make_mesh_with_smooth_normals (geom::triangulation<geom::point<T, 3>, 2, Index> const & triangulation, Weight && weight = Weight())
	{
		struct vertex
		{
			using attributes = std::tuple<
				attribute<0, geom::point<T, 3>, 0>,
				attribute<NormalAttributeIndex, geom::vector<T, 3>, sizeof(geom::point<T, 3>)>
			>;

			geom::point<T, 3> position;
			geom::vector<T, 3> normal;
		};

		std::vector<geom::vector<T, 3>> normals(triangulation.vertices.size(), geom::vector<T, 3>::zero());
		std::vector<T> weights(triangulation.vertices.size(), T());

		for (auto const & s : triangulation.simplices)
		{
			auto v0 = triangulation.vertices[s[0]];
			auto v1 = triangulation.vertices[s[1]];
			auto v2 = triangulation.vertices[s[2]];

			auto n = geom::normalized((v1 - v0) ^ (v2 - v0));
			auto w = weight(v0, v1, v2);

			normals[s[0]] += n * w;
			normals[s[1]] += n * w;
			normals[s[2]] += n * w;

			weights[s[0]] += w;
			weights[s[1]] += w;
			weights[s[2]] += w;
		}

		std::vector<vertex> vertices(triangulation.vertices.size());

		for (std::size_t i = 0; i < triangulation.vertices.size(); ++i)
		{
			vertices[i].position = triangulation.vertices[i];
			vertices[i].normal = (weights[i] != T()) ? (normals[i] / weights[i]) : geom::vector<T, 3>::zero();
		}

		indexed_mesh m;
		m.load(vertices);
		m.load_indices(triangulation.simplices);
		m.mode(GL_TRIANGLES);
		return m;
	}

}
