#pragma once

#include <memory>

namespace xgl
{

	namespace mesh
	{

		namespace detail
		{

			struct any_base
			{
				virtual void draw ( ) = 0;

				virtual ~ any_base ( ) { }
			};

			template <typename Mesh>
			struct any_impl
				: any_base
			{
				Mesh mesh;

				any_impl (Mesh mesh)
					: mesh(std::move(mesh))
				{ }

				void draw ( ) override
				{
					mesh.draw();
				}
			};

		}

		struct any
		{
			any ( ) = default;

			template <typename Mesh>
			any (Mesh mesh)
				: p_(std::make_unique<detail::any_impl<Mesh>>(std::move(mesh)))
			{ }

			explicit operator bool () const
			{
				return static_cast<bool>(p_);
			}

			void draw ( )
			{
				p_->draw();
			}

		private:
			std::unique_ptr<detail::any_base> p_;
		};

	}

}
