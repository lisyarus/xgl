#pragma once

#include <xgl/shaders/base.hpp>

namespace xgl::shaders
{

	struct simple_plain
		: base
	{
		static xgl::program make_program ( );

		simple_plain ( )
			: base(make_program())
		{ }

		void color (geom::vector_4f const & color);
	};

	struct simple_colored
		: base
	{
		static xgl::program make_program ( );

		simple_colored ( )
			: base(make_program())
		{ }
	};

	struct simple_plain_transformed
		: base
	{
		static xgl::program make_program ( );

		simple_plain_transformed ( )
			: base(make_program())
		{ }

		void color (geom::vector_4f const & color);
		void transform (geom::matrix_4x4f const & transform);
	};

	struct simple_colored_transformed
		: base
	{
		static xgl::program make_program ( );

		simple_colored_transformed ( )
			: base(make_program())
		{ }

		void transform (geom::matrix_4x4f const & transform);
	};

}
