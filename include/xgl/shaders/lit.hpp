#pragma once

#include <xgl/shaders/base.hpp>

namespace xgl::shaders
{

	struct lit_plain
		: base
	{
		static xgl::program make_program ( );

		lit_plain ( )
			: base(make_program())
		{ }

		void color (geom::vector_4f const & color);
		void modelview (geom::matrix_4x4f const & transform);
		void projection (geom::matrix_4x4f const & transform);
		void ambient_color (geom::vector_4f const & color);
		void light_color (geom::vector_4f const & color);
		void light_position (geom::vector_4f const & position);
	};

	struct lit_colored
		: base
	{
		static xgl::program make_program ( );

		lit_colored ( )
			: base(make_program())
		{ }

		void modelview (geom::matrix_4x4f const & transform);
		void projection (geom::matrix_4x4f const & transform);
		void ambient_color (geom::vector_4f const & color);
		void light_color (geom::vector_4f const & color);
		void light_position (geom::vector_4f const & position);
	};

}
