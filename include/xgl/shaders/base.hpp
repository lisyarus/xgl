#pragma once

#include <xgl/objects/program.hpp>

namespace xgl::shaders
{

	struct base
	{
		base (xgl::program program)
			: program_(std::move(program))
		{ }

		xgl::program const & program ( )
		{
			return program_;
		}

	protected:
		xgl::program program_;
	};

}
