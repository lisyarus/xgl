#pragma once

#include <xgl/context.hpp>
#include <xgl/objects/object.hpp>

#include <cstdint>
#include <type_traits>
#include <vector>

namespace xgl
{

	namespace detail
	{

		template <typename T>
		std::size_t array_size (std::vector<T> const & a)
		{
			return a.size();
		}

		template <typename T, std::size_t N>
		std::size_t array_size (std::array<T, N> const & a)
		{
			return N;
		}

		template <typename T, std::size_t N>
		std::size_t array_size (T const (&a)[N])
		{
			return N;
		}

		template <typename T>
		T const * array_data (std::vector<T> const & a)
		{
			return a.data();
		}

		template <typename T, std::size_t N>
		T const * array_data (std::array<T, N> const & a)
		{
			return a.data();
		}

		template <typename T, std::size_t N>
		T const * array_data (T const (&a)[N])
		{
			return a;
		}

		template <typename Array>
		struct is_array
		{
		private:

			typedef char true_t;

			struct false_t { char _d[2]; };

			template <typename A>
			static true_t check (decltype(array_data(std::declval<A>())) * , decltype(array_size(std::declval<A>())) *);

			template <typename A>
			static false_t check (...);

		public:
			static constexpr bool value = sizeof(check<Array>(0, 0)) == sizeof(true_t);
		};

		template <typename Array>
		using array_value_type = std::remove_reference_t<decltype(*array_data(std::declval<Array>()))>;

	}

	inline id_t create_buffer_id ( )
	{
		id_t id;
		glGenBuffers(1, &id);
		return id;
	}

	inline void delete_buffer_id (id_t id)
	{
		glDeleteBuffers(1, &id);
	}

	inline void bind_buffer (enum_t type, id_t id)
	{
		context::current->state().bind_buffer(type, id);
	}

	inline id_t bind_buffer (enum_t type)
	{
		return context::current->state().bind_buffer(type);
	}

	struct basic_buffer;

	namespace scope
	{

		struct save_buffer
		{
			save_buffer (enum_t type)
				: type_(type)
				, id_(xgl::bind_buffer(type))
			{ }

			~ save_buffer ( )
			{
				xgl::bind_buffer(type(), id());
			}

			enum_t type ( ) const
			{
				return type_;
			}

			id_t id ( ) const
			{
				return id_;
			}

		private:
			enum_t type_;
			id_t id_;
		};

		struct bind_buffer
			: save_buffer
		{
			bind_buffer (enum_t type, id_t id)
				: save_buffer(type)
			{
				xgl::bind_buffer(type, id);
			}

			bind_buffer (basic_buffer const & b);
		};

	}

	struct basic_buffer
		: typed_object
	{
		using typed_object::typed_object;

		basic_buffer ( )
			: typed_object(create_buffer_id())
		{ }

		basic_buffer (basic_buffer &&) = default;

		basic_buffer & operator = (basic_buffer &&) = default;

		basic_buffer (null_tag)
		{ }

		void reset ( ) override
		{
			delete_buffer_id(id());
			id_ = 0;
		}

		template <typename T>
		void data (T const * p, std::size_t size, enum_t usage = GL_STATIC_DRAW)
		{
			scope::bind_buffer binder(*this);
			glBufferData(type(), size * sizeof(T), p, usage);
		}

		template <typename Array>
			typename std::enable_if<detail::is_array<Array>::value>::type
		data (Array const & array, enum_t usage = GL_STATIC_DRAW)
		{
			data(detail::array_data(array), detail::array_size(array), usage);
		}
	};

	template <enum_t Type>
	struct buffer
		: basic_buffer
	{
		using basic_buffer::basic_buffer;

		buffer ( ) = default;
		buffer (buffer &&) = default;

		buffer & operator = (buffer &&) = default;

		enum_t type ( ) const override
		{
			return Type;
		}
	};

	using array_buffer = buffer<GL_ARRAY_BUFFER>;

	inline scope::bind_buffer::bind_buffer (basic_buffer const & b)
		: bind_buffer(b.type(), b.id())
	{ }

}
