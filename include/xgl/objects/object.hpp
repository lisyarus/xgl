#pragma once

#include <xgl/gl.hpp>

#include <util/noncopyable.hpp>

namespace xgl
{

	constexpr struct null_tag { } null;

	struct object
		: util::noncopyable
	{
	protected:

		object ( )
			: id_(0)
		{ }

		object (id_t id)
			: id_(id)
		{ }

		object (object && other)
			: id_(other.id_)
		{
			other.id_ = 0;
		}

		object & operator = (object && other)
		{
			reset();
			id_ = other.id_;
			other.id_ = 0;
			return *this;
		}

	public:

		id_t id ( ) const
		{
			return id_;
		}

		virtual void reset ( ) { }

		virtual ~ object ( )
		{
			reset();
		}

		explicit operator bool ( ) const
		{
			return id() != 0;
		}

		bool operator ! ( ) const
		{
			return !static_cast<bool>(*this);
		}

	protected:
		id_t id_;
	};

	struct typed_object
		: object
	{
		using object::object;

		virtual enum_t type ( ) const = 0;
	};

}
