#pragma once

#include <xgl/context.hpp>
#include <xgl/objects/object.hpp>
#include <xgl/objects/buffer.hpp>

#include <geom/array.hpp>

namespace xgl
{

	inline id_t create_array_id ( )
	{
		id_t id;
		glGenVertexArrays(1, &id);
		return id;
	}

	inline void delete_array_id (id_t id)
	{
		glDeleteVertexArrays(1, &id);
	}

	inline void bind_array (id_t id)
	{
		context::current->state().bind_array(id);
	}

	inline id_t bind_array ( )
	{
		return context::current->state().bind_array();
	}

	struct array;

	namespace scope
	{

		struct save_array
		{
			save_array ( )
				: id_(xgl::bind_array())
			{ }

			~ save_array ( )
			{
				xgl::bind_array(id());
			}

			id_t id ( ) const
			{
				return id_;
			}

		private:
			id_t id_;
		};

		struct bind_array
			: save_array
		{
			bind_array (id_t id)
			{
				xgl::bind_array(id);
			}

			bind_array (array const & a);
		};

	}

	static constexpr struct normalized_tag_t { } normalized;

	struct array
		: object
	{
		using object::object;

		array ( )
			: object(create_array_id())
		{ }

		array (null_tag)
		{ }

		void reset ( ) override
		{
			delete_array_id(id());
			id_ = 0;
		}

		struct attribute_proxy
		{
			scope::bind_array binder;

			index_t index;

			attribute_proxy & enable (bool e = true)
			{
				if (e)
					glEnableVertexAttribArray(index);
				else
					glDisableVertexAttribArray(index);

				return *this;
			}

			attribute_proxy & disable (bool d = true)
			{
				return enable(!d);
			}

			attribute_proxy & divisor (int d)
			{
				glVertexAttribDivisor(index, d);
				return *this;
			}

			template <typename T>
			typename std::enable_if_t<geom::is_array<T>::value, attribute_proxy &>
				data (std::size_t offset, size_t stride = 0)
			{
				glVertexAttribPointer(index, T::dimension, to_gl_type<typename T::value_type>::value, GL_FALSE, stride, reinterpret_cast<void const *>(offset));
				return *this;
			}

			template <typename T>
			typename std::enable_if_t<!geom::is_array<T>::value, attribute_proxy &>
				data (std::size_t offset, size_t stride = 0, size_t dim = 1)
			{
				glVertexAttribPointer(index, dim, to_gl_type<T>::value, GL_FALSE, stride, reinterpret_cast<void const *>(offset));
				return *this;
			}

			template <typename T>
			typename std::enable_if_t<geom::is_array<T>::value, attribute_proxy &>
				data (std::size_t offset, normalized_tag_t, size_t stride = 0)
			{
				glVertexAttribPointer(index, T::dimension, to_gl_type<typename T::value_type>::value, GL_TRUE, stride, reinterpret_cast<void const *>(offset));
				return *this;
			}

			template <typename T>
			typename std::enable_if_t<!geom::is_array<T>::value, attribute_proxy &>
				data (std::size_t offset, normalized_tag_t, size_t stride = 0, size_t dim = 1)
			{
				glVertexAttribPointer(index, dim, to_gl_type<T>::value, GL_TRUE, stride, reinterpret_cast<void const *>(offset));
				return *this;
			}
		};

		attribute_proxy operator [] (index_t i)
		{
			return {{*this}, i};
		}

		// Fill attribute types & locations from vertex type specification
		// Vertex::attributes is assumed to be a tuple of attribute types
		// An attribute type Attr must have Attr::index, Attr::type and Attr::offset defined
		template <typename Vertex>
		void attributes ( )
		{
			scope::bind_array binder(id());
			attributes_impl(typename Vertex::attributes{}, sizeof(Vertex));
		}

		template <typename Vertex>
		void attributes (array_buffer const & buffer)
		{
			scope::bind_buffer binder(buffer);
			attributes<Vertex>();
		}

		void indices (array_buffer const & index_buffer)
		{
			scope::bind_array binder(id());
			// NB: GL_ELEMENT_ARRAY_BUFFER is part of VAO state,
			// hence is not cached and bound directly through OpenGL
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer.id());
		}

		void draw (enum_t mode, size_t first, size_t count)
		{
			scope::bind_array binder(id());
			glDrawArrays(mode, first, count);
		}

		void draw (enum_t mode, size_t count)
		{
			draw(mode, 0, count);
		}

		template <typename Index>
		void draw_indexed (enum_t mode, size_t index_count, Index const * index_offset = nullptr)
		{
			draw_indexed(mode, index_count, to_gl_type<Index>::value, index_offset);
		}

		void draw_indexed (enum_t mode, size_t index_count, enum_t index_type, void const * index_offset = nullptr)
		{
			scope::bind_array binder(id());
			glDrawElements(mode, index_count, index_type, index_offset);
		}

		void draw_instanced (enum_t mode, size_t first, size_t count, size_t instance_count)
		{
			scope::bind_array binder(id());
			glDrawArraysInstanced(mode, first, count, instance_count);
		}

		void draw_instanced (enum_t mode, size_t count, size_t instance_count)
		{
			draw_instanced(mode, 0, count, instance_count);
		}

	private:

		template <typename ... Attributes>
		void attributes_impl (std::tuple<Attributes ...>, std::size_t stride)
		{
			(attributes_impl_single<Attributes>(stride), ...);
		}

		template <typename Attribute>
		std::enable_if_t<geom::is_array<typename Attribute::type>::value, void>
			attributes_impl_single (std::size_t stride)
		{
			using attr_type = typename Attribute::type;
			using scalar_type = typename attr_type::value_type;
			glEnableVertexAttribArray(Attribute::index);
			glVertexAttribPointer(Attribute::index, attr_type::dimension, to_gl_type<scalar_type>::value, GL_FALSE, stride, reinterpret_cast<void const *>(Attribute::offset));
		}

		template <typename Attribute>
		std::enable_if_t<!geom::is_array<typename Attribute::type>::value, void>
			attributes_impl_single (std::size_t stride)
		{
			glEnableVertexAttribArray(Attribute::index);
			glVertexAttribPointer(Attribute::index, 1, to_gl_type<typename Attribute::type>::value, GL_FALSE, stride, reinterpret_cast<void const *>(Attribute::offset));
		}

	};

	inline scope::bind_array::bind_array (array const & a)
		: bind_array(a.id())
	{ }

}
