#pragma once

#include <xgl/context.hpp>
#include <xgl/objects/object.hpp>
#include <xgl/pixel.hpp>

#include <geom/alias/vector.hpp>
#include <geom/alias/rectangle.hpp>
#include <geom/primitives/ndarray.hpp>

#include <utility>

namespace xgl
{

	inline id_t create_texture_id ( )
	{
		id_t id;
		glGenTextures(1, &id);
		return id;
	}

	inline void delete_texture_id (id_t id)
	{
		glDeleteTextures(1, &id);
	}

	inline void bind_texture (enum_t type, id_t id)
	{
		context::current->state().bind_texture(type, id);
	}

	inline id_t bind_texture (enum_t type)
	{
		return context::current->state().bind_texture(type);
	}

	inline void active_texture (enum_t unit)
	{
		context::current->state().active_texture(unit);
	}

	inline enum_t active_texture ( )
	{
		return context::current->state().active_texture();
	}

	struct basic_texture;

	namespace scope
	{

		struct save_texture
		{
			save_texture (enum_t type)
				: type_(type)
				, id_(xgl::bind_texture(type))
			{ }

			~ save_texture ( )
			{
				xgl::bind_texture(type(), id());
			}

			enum_t type ( ) const
			{
				return type_;
			}

			id_t id ( ) const
			{
				return id_;
			}

		private:
			enum_t type_;
			id_t id_;
		};

		struct bind_texture
			: save_texture
		{
			bind_texture (enum_t type, id_t id)
				: save_texture(type)
			{
				xgl::bind_texture(type, id);
			}

			bind_texture (basic_texture const & texture);
		};

		struct save_texture_unit
		{
			save_texture_unit ( )
				: unit_(xgl::active_texture())
			{ }

			~ save_texture_unit ( )
			{
				xgl::active_texture(unit_);
			}

			enum_t unit ( ) const
			{
				return unit_;
			}

		private:
			enum_t unit_;
		};

		struct bind_texture_unit
			: save_texture_unit
		{
			bind_texture_unit (enum_t unit)
			{
				xgl::active_texture(unit);
			}
		};

		struct bind_texture_to_unit
		{
			bind_texture_to_unit (enum_t unit, enum_t type, id_t id)
			{
				unit_ = unit;
				type_ = type;

				bind_texture_unit binder(unit_);
				id_ = xgl::bind_texture(type);
				xgl::bind_texture(type, id);
			}

			bind_texture_to_unit (enum_t unit, xgl::basic_texture const & texture);

			~ bind_texture_to_unit ( )
			{
				bind_texture_unit binder(unit_);

				xgl::bind_texture(type_, id_);
			}

			enum_t unit ( ) const
			{
				return unit_;
			}

			enum_t type ( ) const
			{
				return type_;
			}

			id_t id ( ) const
			{
				return id_;
			}

		private:
			enum_t unit_;
			enum_t type_;
			enum_t id_;
		};

	}

	struct basic_texture
		: typed_object
	{
		using typed_object::typed_object;

		basic_texture ( )
			: typed_object(create_texture_id())
		{ }

		basic_texture (null_tag)
		{ }

		void reset ( ) override
		{
			delete_texture_id(id());
			id_ = 0;
		}

		void set (GLenum parameter, GLint value)
		{
			scope::bind_texture binder(*this);
			glTexParameteri(type(), parameter, value);
		}

		void set (GLenum parameter, enum_t value)
		{
			scope::bind_texture binder(*this);
			glTexParameteri(type(), parameter, value);
		}

		void set (GLenum parameter, GLint const * value)
		{
			scope::bind_texture binder(*this);
			glTexParameteriv(type(), parameter, value);
		}

		void set (GLenum parameter, GLfloat value)
		{
			scope::bind_texture binder(*this);
			glTexParameterf(type(), parameter, value);
		}

		void set (GLenum parameter, GLfloat const * value)
		{
			scope::bind_texture binder(*this);
			glTexParameterfv(type(), parameter, value);
		}

		void generate_mipmap ( )
		{
			scope::bind_texture binder(*this);
			glGenerateMipmap(type());
		}

		void set_filters (enum_t minification, enum_t magnification)
		{
			scope::bind_texture binder(*this);
			set(GL_TEXTURE_MIN_FILTER, minification);
			set(GL_TEXTURE_MAG_FILTER, magnification);
		}

		void set_wrap (enum_t wrap_s, enum_t wrap_t)
		{
			scope::bind_texture binder(*this);
			set(GL_TEXTURE_WRAP_S, wrap_s);
			set(GL_TEXTURE_WRAP_T, wrap_t);
		}

		enum_t pixel_format ( ) const
		{
			return pixel_format_;
		}

	protected:
		enum_t pixel_format_;
	};

	struct texture_1d
		: basic_texture
	{
		using basic_texture::basic_texture;

		enum_t type ( ) const override
		{
			return GL_TEXTURE_1D;
		}

		size_t width ( ) const
		{
			return size_[0];
		}

		geom::vector_1i size ( ) const
		{
			return size_;
		}

		geom::rectangle_1i rect ( ) const
		{
			return {{{0, size_[0]}}};
		}

		template <typename Pixel>
		void load (geom::vector_1i const & size, std::size_t lod, Pixel const * pixels = nullptr)
		{
			scope::bind_texture binder(*this);

			typedef pixel_traits<Pixel> traits;

			glTexImage1D(type(), lod, traits::gl_internal_format, size[0], 0, traits::gl_format, traits::gl_type, pixels);

			size_ = size;
			pixel_format_ = traits::gl_internal_format;
		}

		template <typename Pixel>
		void load (geom::vector_1i const & size, Pixel const * pixels = nullptr)
		{
			load(size, 0, pixels);
		}

	private:
		geom::vector_1i size_;
	};

	struct texture_2d
		: basic_texture
	{
		using basic_texture::basic_texture;

		enum_t type ( ) const override
		{
			return GL_TEXTURE_2D;
		}

		size_t width ( ) const
		{
			return size_[0];
		}

		size_t height ( ) const
		{
			return size_[1];
		}

		geom::vector_2i size ( ) const
		{
			return size_;
		}

		geom::rectangle_2i rect ( ) const
		{
			return {{{0, size_[0]}, {0, size_[1]}}};
		}

		template <typename Pixel>
		void load (geom::vector_2i const & size, std::size_t lod, Pixel const * pixels = nullptr)
		{
			scope::bind_texture binder(*this);

			typedef pixel_traits<Pixel> traits;

			glTexImage2D(type(), lod, traits::gl_internal_format, size[0], size[1], 0, traits::gl_format, traits::gl_type, pixels);

			size_ = size;
			pixel_format_ = traits::gl_internal_format;
		}

		template <typename Pixel>
		void load (geom::vector_2i const & size, Pixel const * pixels = nullptr)
		{
			load(size, 0, pixels);
		}

		template <typename Pixel>
		void load (geom::ndarray<Pixel, 2> const & array, std::size_t lod = 0)
		{
			load(geom::cast<int>(array.dimensions), lod, array.data());
		}

	private:
		geom::vector_2i size_;
	};

	struct texture_2d_multisample
		: basic_texture
	{
		using basic_texture::basic_texture;

		enum_t type ( ) const override
		{
			return GL_TEXTURE_2D_MULTISAMPLE;
		}

		size_t width ( ) const
		{
			return size_[0];
		}

		size_t height ( ) const
		{
			return size_[1];
		}

		geom::vector_2i size ( ) const
		{
			return size_;
		}

		size_t samples ( ) const
		{
			return samples_;
		}

		geom::rectangle_2i rect ( ) const
		{
			return {{{0, size_[0]}, {0, size_[1]}}};
		}

		template <typename Pixel>
		void load (geom::vector_2i const & size, size_t samples, bool fixed_sample_locations = true)
		{
			scope::bind_texture binder(*this);

			typedef pixel_traits<Pixel> traits;

			glTexImage2DMultisample(type(), samples, traits::gl_internal_format, size[0], size[1], fixed_sample_locations);

			size_ = size;
			pixel_format_ = traits::gl_internal_format;
			samples_ = samples;
		}

	private:
		geom::vector_2i size_;
		size_t samples_;
	};

	struct texture_3d
		: basic_texture
	{
		using basic_texture::basic_texture;

		enum_t type ( ) const override
		{
			return GL_TEXTURE_3D;
		}

		size_t width ( ) const
		{
			return size_[0];
		}

		size_t height ( ) const
		{
			return size_[1];
		}

		size_t depth ( ) const
		{
			return size_[2];
		}

		geom::vector_3i size ( ) const
		{
			return size_;
		}

		geom::rectangle_3i rect ( ) const
		{
			return {{{0, size_[0]}, {0, size_[1]}, {0, size_[2]}}};
		}

		template <typename Pixel>
		void load (geom::vector_3i const & size, std::size_t lod, Pixel const * pixels = nullptr)
		{
			scope::bind_texture binder(*this);

			typedef pixel_traits<Pixel> traits;

			glTexImage3D(type(), lod, traits::gl_internal_format, size[0], size[1], size[2], 0, traits::gl_format, traits::gl_type, pixels);

			size_ = size;
			pixel_format_ = traits::gl_internal_format;
		}

		template <typename Pixel>
		void load (geom::vector_3i const & size, Pixel const * pixels = nullptr)
		{
			load(size, 0, pixels);
		}

	private:
		geom::vector_3i size_;
	};

	struct texture_cube_map
		: basic_texture
	{
		using basic_texture::basic_texture;

		enum_t type ( ) const override
		{
			return GL_TEXTURE_CUBE_MAP;
		}

		template <typename Pixel>
		void load (enum_t face, geom::vector_2i const & size, std::size_t lod, Pixel const * pixels = nullptr)
		{
			scope::bind_texture binder(*this);

			typedef pixel_traits<Pixel> traits;

			glTexImage2D(face, lod, traits::gl_internal_format, size[0], size[1], 0, traits::gl_format, traits::gl_type, pixels);

			pixel_format_ = traits::gl_internal_format;
		}

		template <typename Pixel>
		void load (enum_t face, geom::vector_2i const & size, Pixel const * pixels = nullptr)
		{
			load(face, size, 0, pixels);
		}

	private:
		geom::vector_3i size_;
	};

	inline scope::bind_texture::bind_texture (basic_texture const & texture)
		: bind_texture(texture.type(), texture.id())
	{ }

	inline scope::bind_texture_to_unit::bind_texture_to_unit (enum_t unit, basic_texture const & texture)
		: bind_texture_to_unit(unit, texture.type(), texture.id())
	{ }

}
