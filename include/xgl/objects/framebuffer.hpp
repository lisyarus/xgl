#pragma once

#include <xgl/context.hpp>
#include <xgl/objects/object.hpp>
#include <xgl/objects/texture.hpp>
#include <xgl/objects/renderbuffer.hpp>

namespace xgl
{

	inline id_t create_framebuffer_id ( )
	{
		id_t id;
		glGenFramebuffers(1, &id);
		return id;
	}

	inline void delete_framebuffer_id (id_t id)
	{
		glDeleteFramebuffers(1, &id);
	}

	inline void bind_framebuffer (enum_t type, id_t id)
	{
		context::current->state().bind_framebuffer(type, id);
	}

	inline id_t bind_framebuffer (enum_t type)
	{
		return context::current->state().bind_framebuffer(type);
	}

	struct basic_framebuffer;

	namespace scope
	{

		struct save_framebuffer
		{
			save_framebuffer (enum_t type)
				: type_(type)
				, id_(xgl::bind_framebuffer(type))
			{ }

			~ save_framebuffer ( )
			{
				xgl::bind_framebuffer(type(), id());
			}

			enum_t type ( ) const
			{
				return type_;
			}

			id_t id ( ) const
			{
				return id_;
			}

		private:
			enum_t type_;
			id_t id_;
		};

		struct bind_framebuffer
			: save_framebuffer
		{
			bind_framebuffer (enum_t type, id_t id)
				: save_framebuffer(type)
			{
				xgl::bind_framebuffer(type, id);
			}

			bind_framebuffer (basic_framebuffer const & framebuffer);
			bind_framebuffer (enum_t type, basic_framebuffer const & framebuffer);
		};

	}

	struct basic_framebuffer
		: typed_object
	{
		using typed_object::typed_object;

		basic_framebuffer ( )
			: typed_object(create_framebuffer_id())
		{ }

		basic_framebuffer (null_tag)
		{ }

		void reset ( ) override
		{
			delete_framebuffer_id(id());
			id_ = 0;
		}

		void attach (texture_2d & texture, int attachment = GL_COLOR_ATTACHMENT0)
		{
			scope::bind_framebuffer binder(*this);
			glFramebufferTexture2D(type(), attachment, texture.type(), texture.id(), 0);
		}

		void attach (texture_2d_multisample & texture, int attachment = GL_COLOR_ATTACHMENT0)
		{
			scope::bind_framebuffer binder(*this);
			glFramebufferTexture2D(type(), attachment, texture.type(), texture.id(), 0);
		}

		void attach (renderbuffer & renderbuffer, int attachment = GL_COLOR_ATTACHMENT0)
		{
			scope::bind_framebuffer binder(*this);
			glFramebufferRenderbuffer(type(), attachment, renderbuffer.type(), renderbuffer.id());
		}

		template <typename Target>
		void attach_depth (Target & target)
		{
			attach(target, GL_DEPTH_ATTACHMENT);
		}

		template <typename Target>
		void attach_stencil (Target & target)
		{
			attach(target, GL_STENCIL_ATTACHMENT);
		}

		enum_t status ( ) const
		{
			scope::bind_framebuffer binder(*this);
			return glCheckFramebufferStatus(type());
		}

		bool complete ( ) const
		{
			return status() == GL_FRAMEBUFFER_COMPLETE;
		}
	};

	struct framebuffer
		: basic_framebuffer
	{
		using basic_framebuffer::basic_framebuffer;

		enum_t type ( ) const override
		{
			return GL_FRAMEBUFFER;
		}

		static framebuffer default_framebuffer ( )
		{
			return framebuffer(id_t());
		}
	};

	inline scope::bind_framebuffer::bind_framebuffer (basic_framebuffer const & framebuffer)
		: bind_framebuffer(framebuffer.type(), framebuffer.id())
	{ }

	inline scope::bind_framebuffer::bind_framebuffer (enum_t type, basic_framebuffer const & framebuffer)
		: bind_framebuffer(type, framebuffer.id())
	{ }

}
