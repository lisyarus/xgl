#pragma once

#include <xgl/context.hpp>
#include <xgl/objects/object.hpp>

#include <xgl/pixel.hpp>

#include <geom/alias/vector.hpp>

namespace xgl
{

	inline id_t create_renderbuffer_id ( )
	{
		id_t id;
		glGenRenderbuffers(1, &id);
		return id;
	}

	inline void delete_renderbuffer_id (id_t id)
	{
		glDeleteRenderbuffers(1, &id);
	}

	inline void bind_renderbuffer (enum_t type, id_t id)
	{
		context::current->state().bind_renderbuffer(type, id);
	}

	inline id_t bind_renderbuffer (enum_t type)
	{
		return context::current->state().bind_renderbuffer(type);
	}

	struct renderbuffer;

	namespace scope
	{

		struct save_renderbuffer
		{
			save_renderbuffer (enum_t type)
				: type_(type)
				, id_(xgl::bind_renderbuffer(type))
			{ }

			~ save_renderbuffer ( )
			{
				xgl::bind_renderbuffer(type(), id());
			}

			enum_t type ( ) const
			{
				return type_;
			}

			id_t id ( ) const
			{
				return id_;
			}

		private:
			enum_t type_;
			id_t id_;
		};

		struct bind_renderbuffer
			: save_renderbuffer
		{
			bind_renderbuffer (enum_t type, id_t id)
				: save_renderbuffer(type)
			{
				xgl::bind_renderbuffer(type, id);
			}

			bind_renderbuffer (renderbuffer const & renderbuffer);
		};

	}

	struct renderbuffer
		: typed_object
	{
		using typed_object::typed_object;

		renderbuffer ( )
			: typed_object(create_renderbuffer_id())
		{ }

		renderbuffer (null_tag)
		{ }

		void reset ( ) override
		{
			delete_renderbuffer_id(id());
			id_ = 0;
		}

		enum_t type ( ) const override
		{
			return GL_RENDERBUFFER;
		}

		enum_t pixel_format ( ) const
		{
			return pixel_format_;
		}

		size_t width ( ) const
		{
			return size_[0];
		}

		size_t height ( ) const
		{
			return size_[1];
		}

		geom::vector_2i size ( ) const
		{
			return size_;
		}

		template <typename Pixel>
		void storage (geom::vector_2i size, Pixel const * = nullptr)
		{
			scope::bind_renderbuffer binder(*this);

			typedef pixel_traits<Pixel> traits;

			glRenderbufferStorage(type(), traits::gl_internal_format, size[0], size[1]);

			size_ = size;
			pixel_format_ = traits::gl_internal_format;
		}

	private:
		enum_t pixel_format_;
		geom::vector_2i size_;
	};

	inline scope::bind_renderbuffer::bind_renderbuffer (renderbuffer const & renderbuffer)
		: bind_renderbuffer(renderbuffer.type(), renderbuffer.id())
	{ }

}
