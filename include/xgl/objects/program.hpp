#pragma once

#include <xgl/context.hpp>
#include <xgl/objects/object.hpp>

#include <geom/alias/vector.hpp>
#include <geom/alias/point.hpp>
#include <geom/alias/matrix.hpp>

#include <boost/utility/string_ref.hpp>

#include <string>
#include <cstring>
#include <memory>
#include <stdexcept>
#include <unordered_map>

namespace xgl
{

	struct compillation_error
		: std::runtime_error
	{
		using std::runtime_error::runtime_error;
	};

	struct link_error
		: std::runtime_error
	{
		using std::runtime_error::runtime_error;
	};

	namespace detail
	{

		inline const char * c_str (const char * str)
		{
			return str;
		}

		inline const char * c_str (std::string const & str)
		{
			return str.c_str();
		}

		inline const char * c_str (boost::string_ref str)
		{
			return str.data();
		}

		inline size_t length (const char * str)
		{
			auto l = std::strlen(str);
			if (str[l-1] == 0)
				--l;
			return l;
		}

		inline size_t length (std::string const & str)
		{
			return str.size();
		}

		inline size_t length (boost::string_ref str)
		{
			auto l = str.size();
			if (str[l - 1] == 0)
				--l;
			return l;
		}

		inline std::string to_string (std::unique_ptr<char[]> const & str, size_t length)
		{
			std::string result;
			result.resize(length);
			std::copy(str.get(), str.get() + length, result.begin());
			return result;
		}

	}

	inline id_t create_shader_id (enum_t type)
	{
		return glCreateShader(type);
	}

	inline void delete_shader_id (id_t id)
	{
		glDeleteShader(id);
	}

	inline void bind_program (id_t id)
	{
		context::current->state().use_program(id);
	}

	inline id_t bind_program ( )
	{
		return context::current->state().use_program();
	}

	struct program;

	namespace scope
	{

		struct save_program
		{
			save_program ( )
				: id_(xgl::bind_program())
			{ }

			~ save_program ( )
			{
				xgl::bind_program(id_);
			}

		private:
			id_t id_;
		};

		struct bind_program
			: save_program
		{
			bind_program (id_t id)
			{
				xgl::bind_program(id);
			}

			bind_program (program const & p);
		};

	}

	struct basic_shader
		: typed_object
	{
		using typed_object::typed_object;

		void reset ( ) override
		{
			delete_shader_id(id());
			id_ = 0;
		}

		template <typename ... Sources>
		void compile (Sources const & ... sources)
		{
			compile(std::nothrow, sources...);
			if (!compiled())
				throw compillation_error(info_log());
		}

		template <typename ... Sources>
		void compile (std::nothrow_t, Sources const & ... sources)
		{
			const char * sources_c_str[] = {detail::c_str(sources)...};
			size_t sources_length[] = {detail::length(sources)...};

			glShaderSource(id(), sizeof...(sources), sources_c_str, sources_length);
			glCompileShader(id());
		}

		bool compiled ( ) const
		{
			GLint compile_status;
			glGetShaderiv(id(), GL_COMPILE_STATUS, &compile_status);
			return compile_status == GL_TRUE;
		}

		std::string info_log ( ) const
		{
			std::unique_ptr<char[]> log;
			GLint length;
			glGetShaderiv(id(), GL_INFO_LOG_LENGTH, &length);
			log.reset(new char [length]);
			glGetShaderInfoLog(id(), length, nullptr, log.get());
			return detail::to_string(log, length);
		}
	};

	template <enum_t Type>
	struct shader
		: basic_shader
	{
		using basic_shader::basic_shader;

		template <typename ... Sources>
		shader (Sources const & ... sources)
			: basic_shader(create_shader_id(Type))
		{
			 compile(sources...);
		}

		shader (null_tag)
		{ }

		enum_t type ( ) const override
		{
			return Type;
		}
	};

	using vertex_shader = shader<GL_VERTEX_SHADER>;
	using geometry_shader = shader<GL_GEOMETRY_SHADER>;
	using fragment_shader = shader<GL_FRAGMENT_SHADER>;

	namespace detail
	{

		inline void attach (id_t program_id)
		{ }

		template <typename ... Shaders>
		void attach (id_t program_id, basic_shader const & first, Shaders const & ... shaders)
		{
			glAttachShader(program_id, first.id());
			attach(program_id, shaders...);
		}

		inline void detach (id_t program_id)
		{ }

		template <typename ... Shaders>
		void detach (id_t program_id, basic_shader const & first, Shaders const & ... shaders)
		{
			glDetachShader(program_id, first.id());
			detach(program_id, shaders...);
		}

	}

	namespace uniform
	{
		
		typedef GLint location_t;

		inline void set (location_t location, int v0)
		{
			glUniform1i(location, v0);
		}

		inline void set (location_t location, int v0, int v1)
		{
			glUniform2i(location, v0, v1);
		}

		inline void set (location_t location, int v0, int v1, int v2)
		{
			glUniform3i(location, v0, v1, v2);
		}

		inline void set (location_t location, int v0, int v1, int v2, int v3)
		{
			glUniform4i(location, v0, v1, v2, v3);
		}

		inline void set (location_t location, geom::vector_1i const & v)
		{
			glUniform1i(location, v[0]);
		}

		inline void set (location_t location, geom::vector_2i const & v)
		{
			glUniform2i(location, v[0], v[1]);
		}

		inline void set (location_t location, geom::vector_3i const & v)
		{
			glUniform3i(location, v[0], v[1], v[2]);
		}

		inline void set (location_t location, geom::vector_4i const & v)
		{
			glUniform4i(location, v[0], v[1], v[2], v[3]);
		}

		inline void set (location_t location, float v0)
		{
			glUniform1f(location, v0);
		}

		inline void set (location_t location, float v0, float v1)
		{
			glUniform2f(location, v0, v1);
		}

		inline void set (location_t location, float v0, float v1, float v2)
		{
			glUniform3f(location, v0, v1, v2);
		}

		inline void set (location_t location, float v0, float v1, float v2, float v3)
		{
			glUniform4f(location, v0, v1, v2, v3);
		}

		inline void set (location_t location, geom::vector_1f const & v)
		{
			glUniform1f(location, v[0]);
		}

		inline void set (location_t location, geom::vector_2f const & v)
		{
			glUniform2f(location, v[0], v[1]);
		}

		inline void set (location_t location, geom::vector_3f const & v)
		{
			glUniform3f(location, v[0], v[1], v[2]);
		}

		inline void set (location_t location, geom::vector_4f const & v)
		{
			glUniform4f(location, v[0], v[1], v[2], v[3]);
		}

		inline void set (location_t location, geom::point_1f const & v)
		{
			glUniform1f(location, v[0]);
		}

		inline void set (location_t location, geom::point_2f const & v)
		{
			glUniform2f(location, v[0], v[1]);
		}

		inline void set (location_t location, geom::point_3f const & v)
		{
			glUniform3f(location, v[0], v[1], v[2]);
		}

		inline void set (location_t location, geom::point_4f const & v)
		{
			glUniform4f(location, v[0], v[1], v[2], v[3]);
		}

		inline void set (location_t location, geom::matrix_2x2f const & m)
		{
			glUniformMatrix2fv(location, 1, GL_TRUE, m.data()->data());
		}

		inline void set (location_t location, geom::matrix_3x3f const & m)
		{
			glUniformMatrix3fv(location, 1, GL_TRUE, m.data()->data());
		}

		inline void set (location_t location, geom::matrix_4x4f const & m)
		{
			glUniformMatrix4fv(location, 1, GL_TRUE, m.data()->data());
		}

	}

	struct program
		: object
	{
		using object::object;

		template <typename ... Shaders>
		program (Shaders const & ... shaders)
			: object(glCreateProgram())
		{
			link(shaders...);
		}

		program (null_tag)
		{ }

		void reset ( ) override
		{
			glDeleteProgram(id());
			id_ = 0;
			uniforms_.clear();
		}

		template <typename ... Shaders>
		void link (Shaders const & ... shaders)
		{
			link(std::nothrow, shaders...);
			if (!linked())
				throw link_error(info_log());
		}

		template <typename ... Shaders>
		void link (std::nothrow_t, Shaders const & ... shaders)
		{
			uniforms_.clear();

			detail::attach(id(), shaders...);
			glLinkProgram(id());
			detail::detach(id(), shaders...);
		}

		bool linked ( ) const
		{
			GLint link_status;
			glGetProgramiv(id(), GL_LINK_STATUS, &link_status);
			return link_status == GL_TRUE;
		}

		std::string info_log ( ) const
		{
			std::unique_ptr<char[]> log;
			GLint length;
			glGetProgramiv(id(), GL_INFO_LOG_LENGTH, &length);
			log.reset(new char [length]);
			glGetProgramInfoLog(id(), length, nullptr, log.get());
			return detail::to_string(log, length);
		}

		uniform::location_t uniform_location (std::string const & name)
		{
			xgl::uniform::location_t location;

			auto it = uniforms_.find(name);
			if (it == uniforms_.end())
			{
				location = glGetUniformLocation(id(), name.data());
				uniforms_[name] = location;
			}
			else
			{
				location = it->second;
			}

			return location;
		}

		struct uniform_proxy
		{
			scope::bind_program binder;
			uniform::location_t location;

			template <typename T>
			uniform_proxy & operator = (T const & value)
			{
				uniform::set(location, value);
				return *this;
			}

			template <typename ... T>
			uniform_proxy & set (T const & ... value)
			{
				uniform::set(location, value...);
				return *this;
			}
		};

		uniform_proxy operator[] (std::string const & name)
		{
			return {{*this}, uniform_location(name)};
		}

		uniform_proxy operator[] (xgl::uniform::location_t location)
		{
			return {{*this}, location};
		}

	private:
		std::unordered_map<std::string, xgl::uniform::location_t> uniforms_;
	};

	inline scope::bind_program::bind_program (program const & p)
		: bind_program(p.id())
	{ }

}
