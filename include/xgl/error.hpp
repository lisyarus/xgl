#pragma once

#include <xgl/gl.hpp>

#include <stdexcept>

namespace xgl
{

	inline const char * to_string (enum_t error)
	{
		switch (error)
		{
		case GL_NO_ERROR: return "no error";
		case GL_INVALID_ENUM: return "GL_INVALID_ENUM";
		case GL_INVALID_VALUE: return "GL_INVALID_VALUE";
		case GL_INVALID_OPERATION: return "GL_INVALID_OPERATION";
		case GL_STACK_OVERFLOW: return "GL_STACK_OVERFLOW";
		case GL_STACK_UNDERFLOW: return "GL_STACK_UNDERFLOW";
		case GL_OUT_OF_MEMORY: return "GL_OUT_OF_MEMORY";
		default: return "unknown error";
		}
	}

	struct exception
		: std::runtime_error
	{
		exception (enum_t error, std::string const & message)
			: std::runtime_error(to_string(error) + message)
			, error_(error)
		{ }

		enum_t error ( ) const
		{
			return error_;
		}

	private:
		enum_t error_;
	};

	inline enum_t error ( )
	{
		return glGetError();
	}

	inline void throw_error (std::string message = {})
	{
		enum_t e = error();
		if (e != GL_NO_ERROR)
			throw exception(e, message);
	}

}
