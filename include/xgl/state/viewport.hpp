#pragma once

#include <xgl/context.hpp>

#include <geom/alias/rectangle.hpp>

namespace xgl
{

	inline geom::rectangle_2i viewport ( )
	{
		return context::current->state().viewport();
	}

	inline void viewport (geom::rectangle_2i const & r)
	{
		context::current->state().viewport(r);
	}

	namespace scope
	{

		struct save_viewport
		{
			save_viewport ( )
				: r_(viewport())
			{ }

			~ save_viewport ( )
			{
				xgl::viewport(r_);
			}

		private:
			geom::rectangle_2i r_;
		};

		struct set_viewport
			: save_viewport
		{
			set_viewport (geom::rectangle_2i const & r)
			{
				xgl::viewport(r);
			}
		};

	}

}
