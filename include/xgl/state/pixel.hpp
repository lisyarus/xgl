#pragma once

#include <xgl/context.hpp>

namespace xgl
{

	namespace scope
	{

		struct save_pixel_store
		{
			save_pixel_store (enum_t cap)
				: cap_(cap)
				, value_(context::current->state().pixel_store(cap))
			{ }

			~ save_pixel_store ( )
			{
				context::current->state().pixel_store(cap_, value_);
			}

		private:
			enum_t cap_;
			enum_t value_;
		};

		struct set_pixel_store
			: save_pixel_store
		{
			set_pixel_store (enum_t cap, enum_t value)
				: save_pixel_store(cap)
			{
				context::current->state().pixel_store(cap, value);
			}
		};

	}

}
