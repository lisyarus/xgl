#pragma once

#include <xgl/context.hpp>

namespace xgl
{

	namespace scope
	{

		struct save_blend_color
		{
			save_blend_color ( )
				: color_(context::current->state().blend_color())
			{ }

			~ save_blend_color ( )
			{
				context::current->state().blend_color(color_);
			}

		private:
			geom::vector_4f color_;
		};

		struct set_blend_color
			: save_blend_color
		{
			set_blend_color (geom::vector_4f const & color)
			{
				context::current->state().blend_color(color);
			}
		};

		struct save_blend_func
		{
			save_blend_func ( )
				: data_(context::current->state().blend_func())
			{ }

			~ save_blend_func ( )
			{
				context::current->state().blend_func(data_);
			}

		private:
			state::blend_func_data data_;
		};

		struct set_blend_func
			: save_blend_func
		{
			set_blend_func (enum_t src_func, enum_t dst_func)
			{
				context::current->state().blend_func(src_func, dst_func);
			}

			set_blend_func (enum_t src_rgb_func, enum_t dst_rgb_func, enum_t src_alpha_func, enum_t dst_alpha_func)
			{
				context::current->state().blend_func(src_rgb_func, dst_rgb_func, src_alpha_func, dst_alpha_func);
			}
		};

		struct save_blend_equation
		{
			save_blend_equation ( )
				: equation_(context::current->state().blend_equation())
			{ }

			~ save_blend_equation ( )
			{
				context::current->state().blend_equation(equation_);
			}

		private:
			enum_t equation_;
		};

		struct set_blend_equation
			: save_blend_equation
		{
			set_blend_equation (enum_t equation)
			{
				context::current->state().blend_equation(equation);
			}
		};

	}

}
