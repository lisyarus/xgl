#pragma once

#include <xgl/context.hpp>

namespace xgl
{

	inline bool enable (enum_t cap)
	{
		return context::current->state().enable(cap);
	}

	inline void enable (enum_t cap, bool enable)
	{
		context::current->state().enable(cap, enable);
	}

	inline bool enable_client_state (enum_t cap)
	{
		return context::current->state().enable_client_state(cap);
	}

	inline void enable_client_state (enum_t cap, bool enable)
	{
		return context::current->state().enable_client_state(cap, enable);
	}

	namespace scope
	{

		namespace detail
		{

			template <bool Enable>
			struct enable
			{
				enable (enum_t cap)
					: cap_(cap)
					, state_(xgl::enable(cap))
				{
					xgl::enable(cap_, Enable);
				}

				~ enable ( )
				{
					xgl::enable(cap_, state_);
				}

			private:
				enum_t cap_;
				bool state_;
			};

			template <bool Enable>
			struct enable_client_state
			{
				enable_client_state (enum_t cap)
					: cap_(cap)
					, state_(xgl::enable_client_state(cap))
				{
					xgl::enable_client_state(cap_, Enable);
				}

				~ enable_client_state ( )
				{
					xgl::enable_client_state(cap_, state_);
				}

			private:
				enum_t cap_;
				bool state_;
			};

		}

		using enable = detail::enable<true>;

		using disable = detail::enable<false>;

		using enable_client_state = detail::enable_client_state<true>;

		using disable_client_state = detail::enable_client_state<false>;

	}

}
