#pragma once

#include <xgl/context.hpp>

namespace xgl
{

	namespace scope
	{

		struct save_cull_face
		{
			save_cull_face ( )
				: face_(context::current->state().cull_face())
			{ }

			~ save_cull_face ( )
			{
				context::current->state().cull_face(face_);
			}

		private:
			enum_t face_;
		};

		struct set_cull_face
			: save_cull_face
		{
			set_cull_face (enum_t face)
			{
				context::current->state().cull_face(face);
			}
		};

	}

}
