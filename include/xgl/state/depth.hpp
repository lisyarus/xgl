#pragma once

#include <xgl/context.hpp>

namespace xgl
{

	namespace scope
	{

		struct save_depth_range
		{
			save_depth_range ( )
				: range_(context::current->state().depth_range())
			{ }

			~ save_depth_range ( )
			{
				context::current->state().depth_range(range_);
			}

		private:
			geom::interval_d range_;
		};

		struct set_depth_range
			: save_depth_range
		{
			set_depth_range (geom::interval_d const & range)
			{
				context::current->state().depth_range(range);
			}
		};

		struct save_depth_func
		{
			save_depth_func ( )
				: func_(context::current->state().depth_func())
			{ }

			~ save_depth_func ( )
			{
				context::current->state().depth_func(func_);
			}

		private:
			enum_t func_;
		};

		struct set_depth_func
			: save_depth_func
		{
			set_depth_func (enum_t func)
			{
				context::current->state().depth_func(func);
			}
		};

	}

}
