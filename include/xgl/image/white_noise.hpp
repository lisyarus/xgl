#pragma once

#include <xgl/pixel.hpp>
#include <xgl/objects/texture.hpp>
#include <xgl/state/pixel.hpp>

#include <random>
#include <memory>

namespace xgl::image
{

	template <typename RNG>
	void white_noise (xgl::texture_2d & output, geom::vector_2i const & size, RNG && rng)
	{
		std::unique_ptr<pixel_1b[]> pixels(new pixel_1b[size[0] * size[1]]);

		std::uniform_int_distribution<unsigned char> d;
		for (std::size_t y = 0; y < size[1]; ++y)
		{
			for (std::size_t x = 0; x < size[0]; ++x)
			{
				pixels[y * size[0] + x].r = d(rng);
			}
		}

		scope::set_pixel_store set_pixel_store(GL_UNPACK_ALIGNMENT, 1);
		output.load(size, pixels.get());
	}

	template <typename RNG>
	xgl::texture_2d white_noise (geom::vector_2i const & size, RNG && rng)
	{
		auto output = xgl::texture_2d();
		white_noise(output, size, std::forward<RNG>(rng));
		return output;
	}

}
