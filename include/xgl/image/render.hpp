#pragma once

#include <xgl/objects/framebuffer.hpp>
#include <xgl/state/viewport.hpp>

namespace xgl::image
{

	struct render_to_texture
	{
		render_to_texture (framebuffer & f, texture_2d & tex)
			: framebuffer_binder(f)
			, viewport_setter(tex.rect())
		{
			f.attach(tex);
			if (!f.complete())
				throw std::runtime_error("Framebuffer not complete");
		}

	private:
		scope::bind_framebuffer framebuffer_binder;
		scope::set_viewport viewport_setter;
	};

}
