#pragma once

#include <xgl/objects/program.hpp>
#include <xgl/objects/framebuffer.hpp>
#include <xgl/mesh/mesh.hpp>

namespace xgl::image
{

	struct perlin
	{
		perlin ( );

		void operator() (xgl::texture_2d & output, xgl::texture_2d const & noise, geom::vector_2i size);

		xgl::texture_2d operator() (xgl::texture_2d const & noise, geom::vector_2i size)
		{
			auto output = xgl::texture_2d();
			operator()(output, noise, size);
			return output;
		}

	private:
		program program_;
		framebuffer framebuffer_;
		mesh::mesh mesh_;
	};

}
