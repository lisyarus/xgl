#pragma once

#include <xgl/objects/program.hpp>
#include <xgl/objects/texture.hpp>
#include <xgl/objects/framebuffer.hpp>
#include <xgl/mesh/mesh.hpp>

namespace xgl::image
{

	struct fractal
	{
		fractal ( );

		void operator() (xgl::texture_2d & output, xgl::texture_2d const & noise, size_t iterations);

		xgl::texture_2d operator() (xgl::texture_2d const & noise, size_t iterations)
		{
			auto output = xgl::texture_2d();
			operator()(output, noise, iterations);
			return output;
		}

	private:
		program program_;
		framebuffer framebuffer_;
		mesh::mesh mesh_;
	};

}
