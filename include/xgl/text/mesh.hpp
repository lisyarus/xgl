#pragma once

#include <xgl/text/glyph.hpp>
#include <xgl/text/options.hpp>
#include <xgl/mesh/mesh.hpp>

namespace xgl::text
{

	struct mesh
	{
		mesh ( ) = default;

		mesh (null_tag tag)
			: mesh_(tag)
		{ }

		mesh (xgl::text::glyph_atlas const & atlas, std::u32string const & str, options const & opts = {})
		{
			update(atlas, str, opts);
		}

		void update (xgl::text::glyph_atlas const & atlas, std::u32string const & str, options const & opts = {});

		void render ( );

	private:
		xgl::mesh::indexed_mesh mesh_;
	};

}
