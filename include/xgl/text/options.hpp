#pragma once

#include <geom/alias/vector.hpp>
#include <geom/alias/point.hpp>

#include <optional>

namespace xgl::text
{

	struct options
	{
		geom::point_2f start = { 0.f, 0.f };
		geom::vector_2f size = { 1.f, 1.f };
		float spacing = 1.f;
		float line_spacing = 1.f;
		bool up = false;
		std::optional<float> max_row_width;
	};

}
