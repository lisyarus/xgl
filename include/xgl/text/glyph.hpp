#pragma once

#include <xgl/atlas.hpp>

namespace xgl::text
{

	struct glyph_data
	{
		geom::vector_2f offset;
		geom::vector_2f size;
		geom::vector_2f advance;

		geom::rectangle_2f texcoords;
	};

	using glyph_atlas = xgl::basic_atlas<xgl::texture_2d, char32_t, glyph_data>;

}
