#pragma once

#include <xgl/gl.hpp>

#include <geom/alias/vector.hpp>
#include <geom/alias/interval.hpp>
#include <geom/alias/rectangle.hpp>

#include <memory>

namespace xgl
{

	struct state
	{
		struct blend_func_data
		{
			struct
			{
				enum_t rgb;
				enum_t alpha;
			} src, dst;
		};

		virtual void   cull_face (enum_t face) = 0;
		virtual enum_t cull_face ( ) const = 0;

		virtual void pixel_store (enum_t cap, int value) = 0;
		virtual int  pixel_store (enum_t cap) const = 0;

		virtual void depth_range (geom::interval_d const & range) = 0;
		virtual geom::interval_d depth_range ( ) const = 0;

		virtual void   depth_func (enum_t func) = 0;
		virtual enum_t depth_func ( ) const = 0;

		virtual void blend_color (geom::vector_4f const & color) = 0;
		virtual geom::vector_4f blend_color ( ) const = 0;

		virtual void blend_func (enum_t src_func, enum_t dst_func) = 0;
		virtual void blend_func (enum_t src_rgb_func, enum_t dst_rgb_func, enum_t src_alpha_func, enum_t dst_alpha_func) = 0;
		virtual void blend_func (blend_func_data const & data) = 0;
		virtual blend_func_data blend_func( ) const = 0;

		virtual void   blend_equation (enum_t equation) = 0;
		virtual enum_t blend_equation ( ) const = 0;

		virtual void enable (enum_t cap, bool value) = 0;
		virtual bool enable (enum_t cap) const = 0;

		virtual void enable_client_state (enum_t cap, bool value) = 0;
		virtual bool enable_client_state (enum_t cap) const = 0;

		virtual void viewport (geom::rectangle_2i rect) = 0;
		virtual geom::rectangle_2i viewport ( ) const = 0;

		virtual void bind_texture (enum_t type, id_t id) = 0;
		virtual id_t bind_texture (enum_t type) const = 0;

		virtual void   active_texture (enum_t unit) = 0;
		virtual enum_t active_texture ( ) const = 0;

		virtual void bind_renderbuffer (enum_t type, id_t id) = 0;
		virtual id_t bind_renderbuffer (enum_t type) const = 0;

		virtual void bind_framebuffer (enum_t type, id_t id) = 0;
		virtual id_t bind_framebuffer (enum_t type) const = 0;

		virtual void bind_buffer (enum_t type, id_t id) = 0;
		virtual id_t bind_buffer (enum_t type) const = 0;

		virtual void bind_array (id_t id) = 0;
		virtual id_t bind_array ( ) const = 0;

		virtual void use_program (id_t id) = 0;
		virtual id_t use_program ( ) const = 0;

		virtual ~ state ( ) { }
	};

	struct context
	{
		static thread_local context * current;

		virtual void make_current ( ) = 0;

		virtual xgl::state & state ( ) = 0;
		virtual xgl::state const & state ( ) const = 0;

		virtual ~ context ( ) { }
	};

	std::unique_ptr<state> make_cached_state ( );

	std::unique_ptr<context> make_global_context (std::unique_ptr<state>);

}
