#pragma once

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

#include <cstdint>

namespace xgl
{

	typedef GLenum enum_t;
	typedef GLsizei size_t;

	typedef GLuint id_t;
	typedef GLuint index_t;

	inline enum_t to_gl (bool b)
	{
		return b ? GL_TRUE : GL_FALSE;
	}

	template <typename T>
	struct to_gl_type;

	template < >
	struct to_gl_type<char>
	{
		static constexpr enum_t value = GL_BYTE;
	};

	template < >
	struct to_gl_type<unsigned char>
	{
		static constexpr enum_t value = GL_UNSIGNED_BYTE;
	};

	template < >
	struct to_gl_type<short>
	{
		static constexpr enum_t value = GL_SHORT;
	};

	template < >
	struct to_gl_type<unsigned short>
	{
		static constexpr enum_t value = GL_UNSIGNED_SHORT;
	};

	template < >
	struct to_gl_type<int>
	{
		static constexpr enum_t value = GL_INT;
	};

	template < >
	struct to_gl_type<unsigned int>
	{
		static constexpr enum_t value = GL_UNSIGNED_INT;
	};

	template < >
	struct to_gl_type<float>
	{
		static constexpr enum_t value = GL_FLOAT;
	};

	template < >
	struct to_gl_type<double>
	{
		static constexpr enum_t value = GL_DOUBLE;
	};

}
