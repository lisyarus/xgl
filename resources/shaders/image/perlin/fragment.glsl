#version 330

uniform ivec2 noise_size;
uniform vec2 noise_delta;
uniform sampler2D grad;

in vec2 texcoord;

float s(float t){ return t * t * (3.0 - 2.0 * t); }

vec2 fetch (ivec2 g)
{
	if (g.x >= noise_size.x)
		g.x -= noise_size.x;
	if (g.y >= noise_size.y)
		g.y -= noise_size.y;
	float a = texelFetch(grad, g, 0).r * 2.0 * 3.1415926535;
	return vec2(cos(a), sin(a));
}

void main ( )
{
	ivec2 g = ivec2(texcoord / noise_delta);
	vec2 f = texcoord / noise_delta - vec2(g);
	float v00 = dot(f - vec2(0.0, 0.0), fetch(g + ivec2(0, 0)));
	float v01 = dot(f - vec2(0.0, 1.0), fetch(g + ivec2(0, 1)));
	float v10 = dot(f - vec2(1.0, 0.0), fetch(g + ivec2(1, 0)));
	float v11 = dot(f - vec2(1.0, 1.0), fetch(g + ivec2(1, 1)));
	float v0 = mix(v00, v01, s(f.y));
	float v1 = mix(v10, v11, s(f.y));
	float v = mix(v0, v1, s(f.x));
	v = v * 0.8 + 0.5;
	gl_FragColor = vec4(v);
}
