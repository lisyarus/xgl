#version 330

uniform sampler2D noise;
uniform int iterations;

in vec2 texcoord;

void main ( )
{
	vec4 color = vec4(0.0, 0.0, 0.0, 0.0);
	float sum = 0.0;
	float factor = 1.0;
	for (int i = 0; i < iterations; ++i)
	{
		factor /= 2.0;
		color += texture(noise, texcoord / factor / 2.0) * factor;
		sum += factor;
	}
	gl_FragColor = color / sum;
}
