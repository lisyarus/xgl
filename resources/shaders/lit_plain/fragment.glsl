#version 330

uniform vec4 color;
uniform vec4 ambient_color;
uniform vec4 light_color;
uniform vec4 light_position;

in vec3 position_v;
in vec3 normal_v;

void main ( )
{
	float t = max(0.0, dot(normalize(normal_v), normalize(light_position.xyz - position_v * light_position.w)));

	gl_FragColor = color * mix(ambient_color, light_color, t);
}
