#version 330

uniform mat4 transform;

layout (location = 0) in vec4 position;
layout (location = 1) in vec4 color;

out vec4 color_v;

void main ( )
{
	gl_Position = transform * position;
	color_v = color;
}
