#version 330

layout (location = 0) in vec2 vertex_in;

out vec2 texcoord;

void main ( )
{
	gl_Position = vec4(vertex_in, 0.0, 1.0);

	texcoord = vertex_in * 0.5 + vec2(0.5, 0.5);
}
