#version 330

uniform mat4 modelview;
uniform mat4 projection;

layout (location = 0) in vec4 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec4 color;

out vec3 position_v;
out vec3 normal_v;
out vec4 color_v;

void main ( )
{
	vec4 position_world = modelview * position;
	gl_Position = projection * position_world;
	position_v = position_world.xyz;
	normal_v = (modelview * vec4(normal, 0.0)).xyz;
	color_v = color;
}
