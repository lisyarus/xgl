#version 330

in vec4 color_v;

void main ( )
{
	gl_FragColor = color_v;
}
