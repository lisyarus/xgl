#include <xgl/text/freetype.hpp>

#include <xgl/state/pixel.hpp>

#include <geom/operations/range/interval.hpp>
#include <geom/primitives/ndarray.hpp>

#include <freetype2/ft2build.h>
#include FT_FREETYPE_H

#include <util/autoname.hpp>

#include <stdexcept>

namespace xgl::text::freetype
{

	namespace
	{

		struct ft_face
		{
			FT_Face face;

			FT_Face & operator * ()
			{
				return face;
			}

			FT_Face operator -> ( )
			{
				return face;
			}

			ft_face ( )
				: face(nullptr)
			{ }

			~ ft_face ( )
			{
				FT_Done_Face(face);
			}
		};

	}

	struct library::handle
	{
		FT_Library library;
	};

	library::library ( )
		: handle_(std::make_unique<handle>())
	{
		if (FT_Init_FreeType(&get_handle().library))
			throw std::runtime_error("failed to initialize freetype library");
	}

	library::~ library ( )
	{
		FT_Done_FreeType(handle().library);
	}

	glyph_atlas library::load(boost::string_ref path, std::size_t face_index, std::size_t width, std::size_t height, bool antialiased)
	{
		ft_face face;

		if (FT_New_Face(get_handle().library, path.data(), face_index, &(*face)))
			throw std::runtime_error("failed to load face from " + path.to_string());

		if (FT_Set_Pixel_Sizes(*face, width, height))
			throw std::runtime_error("failed to set pixel sizes to (" + std::to_string(width) + "," + std::to_string(height) + ")");

		std::size_t const extra_pixels = 4;

		geom::ndarray<xgl::pixel_1b, 2> pixels;

		geom::interval<std::size_t> char_range{32, 128};

		glyph_atlas atlas;

		// An approximation to the size of the resulting texture
		std::size_t const row_size = std::sqrt(char_range.length() * width * height);
		pixels.resize({row_size, row_size});

		float fwidth = width * 64.f;

		std::size_t start_x = 0;
		std::size_t start_y = 0;
		std::size_t current_row_height = 0;

		for (char32_t c : geom::range(char_range))
		{
			auto char_index = FT_Get_Char_Index(*face, c);

			FT_Load_Glyph(*face, char_index, FT_LOAD_DEFAULT);

			FT_Render_Glyph(face->glyph, antialiased ? FT_RENDER_MODE_NORMAL : FT_RENDER_MODE_MONO);

			glyph_data g;

			std::size_t bitmap_width = face->glyph->bitmap.width;
			std::size_t bitmap_height = face->glyph->bitmap.rows;

			auto read_pixel = [&](std::size_t x, std::size_t y) -> unsigned char
			{
				unsigned char const * row = face->glyph->bitmap.buffer + y * face->glyph->bitmap.pitch;

				if (face->glyph->bitmap.pixel_mode == FT_PIXEL_MODE_GRAY)
				{
					return row[x];
				}
				else if (face->glyph->bitmap.pixel_mode == FT_PIXEL_MODE_MONO)
				{
					unsigned char b = row[x / 8];
					return (b & (1 << (7 - (x % 8)))) == 0 ? 0 : 255;
				}
				else
					throw std::runtime_error("Pixel mode not supported");
			};

			g.offset = { face->glyph->metrics.horiBearingX / 64.f, (face->glyph->metrics.horiBearingY - face->glyph->metrics.height) / 64.f };
			g.size = { face->glyph->metrics.width / 64.f, face->glyph->metrics.height / 64.f };
			g.advance = { face->glyph->metrics.horiAdvance / 64.f, 0 };
			g.texcoords = geom::cast<float>(geom::rectangle_2<std::size_t>{{ {start_x + extra_pixels, start_x + bitmap_width + extra_pixels}, {start_y + extra_pixels, start_y + bitmap_height + extra_pixels} }});

			atlas.map[c] = g;

			current_row_height = std::max(current_row_height, bitmap_height);

			pixels.resize({ std::max(pixels.dimensions[0], start_x + bitmap_width + extra_pixels * 2), std::max(pixels.dimensions[1], start_y + current_row_height + extra_pixels * 2) });

			for (std::size_t x = 0; x < bitmap_width; ++x)
			{
				for (std::size_t y = 0; y < bitmap_height; ++y)
				{
					pixels.at(start_x + x + extra_pixels, start_y + y + extra_pixels).r = read_pixel(x, y);
				}
			}

			start_x += bitmap_width + extra_pixels * 2;
			if (start_x >= row_size)
			{
				start_x = 0;
				start_y += current_row_height + extra_pixels * 2;
				current_row_height = 0;
			}
		}

		atlas.texture = xgl::texture_2d();
		{
			xgl::scope::set_pixel_store st(GL_UNPACK_ALIGNMENT, 1);
			xgl::scope::bind_texture autoname(atlas.texture);
			if (antialiased)
				atlas.texture.set_filters(GL_LINEAR, GL_LINEAR);
			else
				atlas.texture.set_filters(GL_NEAREST, GL_NEAREST);
			atlas.texture.load(geom::cast<int>(geom::vector_2<std::size_t>{pixels.dimensions[0], pixels.dimensions[1]}), pixels.data());
		}

		float x_mult = 1.f / pixels.dimensions[0];
		float y_mult = 1.f / pixels.dimensions[1];

		for (auto & [c, data] : atlas.map)
		{
			data.texcoords[0].inf *= x_mult;
			data.texcoords[0].sup *= x_mult;

			data.texcoords[1].inf *= y_mult;
			data.texcoords[1].sup *= y_mult;
		}

		return atlas;
	}

}
