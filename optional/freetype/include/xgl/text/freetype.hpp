#pragma once

#include <xgl/text/glyph.hpp>

#include <boost/utility/string_ref.hpp>

#include <memory>

namespace xgl::text::freetype
{

	struct library
	{
		library ( );
		~ library ( );

		operator bool ( ) const
		{
			return static_cast<bool>(handle_);
		}

		bool operator ! ( ) const
		{
			return !handle_;
		}

		glyph_atlas load (boost::string_ref path, std::size_t face_index, std::size_t width, std::size_t height, bool antialiased = true);

	private:
		struct handle;

		std::unique_ptr<handle> handle_;

		handle & get_handle ( )
		{
			return *handle_;
		}
	};

}
